{- |
   Module      : Main
   Description : Entry point for the application.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Main
    ( main  -- :: IO ()
    ) where

import Core.CLI
    ( Commands(Binarise, Debinarise, Induce, Outside, Parse, Smooth, Unk)
    , Paradigm(Deductive)
    , ParseOptions(ParseOptions, kbest, paradigm, smoothing)
    , commands
    )
import Grammar.Binarise (runBinarise)
import Grammar.Debinarise (runDebinarise)
import Grammar.Induction (runInduction)
import Grammar.Unking (runUnking)
import Parse.Command (runParse)
import Parse.Outside (runOutside)

import Options.Applicative (ParserPrefs, customExecParser, helpLongEquals, prefs)
import System.Exit (ExitCode(ExitFailure))


{- | Entry point for the program.

   Default behaviour if no command line arguments are given: Perform a grammar
   induction on the input given on stdin and print the results to stdout.
-}
main :: IO ()
main =
    maybe (runInduction Nothing) decideAction =<< customExecParser ps commands
  where
    ps :: ParserPrefs
    ps = prefs helpLongEquals

-- | Based on the given command, decide what 'IO' action to perform.
decideAction :: Commands -> IO ()
decideAction = \case
    Induce mbF      -> runInduction mbF
    Parse opts r l  -> if   or (missing opts)
                       then notImplemented
                       else runParse opts r l
    Binarise h v    -> runBinarise h v
    Debinarise      -> runDebinarise
    Unk thresh      -> runUnking thresh
    Outside s r l f -> runOutside s r l f
    Smooth{}        -> notImplemented
  where
    -- | Options that are not implemented.
    missing :: ParseOptions -> [Bool]
    missing ParseOptions{ paradigm, smoothing, kbest } =
        [paradigm /= Deductive, smoothing, kbest /= 1]

-- | Exit code 22 for a feature that's not implemented.
notImplemented :: IO a
notImplemented = exitWith (ExitFailure 22)
