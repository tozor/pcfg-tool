{- |
   Module      : Grammar.UtilSpec
   Description : Unit and property tests for functions in 'Grammar.Util'
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.UtilSpec
    ( spec  -- :: Spec
    ) where

import Core.Tree (PennForest)
import Core.Util (Horizontal(Horizontal), Vertical(Vertical))
import Grammar.Parser (pTree)
import Grammar.Util (binarise, debinarise, insertRulesInto, normalize)
import Util (aPosInt, treeGen)

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as Map

import Data.Attoparsec.ByteString.Char8 (parseOnly)
import Test.Hspec (Spec, context, describe, it, shouldBe)
import Test.QuickCheck (forAll)


-- | Specs to export.
spec :: Spec
spec = normalizeSpec
    *> binDebinSpec

{- | In absence of good invariants with which to test our functions, we have to
   resort to unit tests.  This is a very small test and thusly probably not
   adequate for anything.  Note that this function also indirectly (and thus
   perhaps not sufficiently) tests 'getOccurrences' and 'insertRulesInto'.

   Note that we have chosen to indent specs by two spaces instead of the usual
   four for readability reasons.
-}
normalizeSpec :: Spec
normalizeSpec =
  describe "normalize" $
    context "given a small test case" $
      it "should produce the desired result" $
        -- Formatting of the weights.
        ( sort . map snd . toList

        -- What we actually want to test.
        . normalize

        -- Getting the grammar.
        . foldr (Map.unionWith (+) . insertRulesInto mempty) mempty) tForest

          `shouldBe`

        -- Calculated by hand, see Note [Rules].
        sort [ 1
             , 0.20408163265306123
             , 0.12244897959183673
             , 0.2653061224489796
             , 0.3673469387755102
             , 0.04081632653061224
             ]

-- | Unit tests for the binarise/debinarise interplay.
binDebinSpec :: Spec
binDebinSpec =
  describe "binarise and debinarise" $
    it "should satisfy debinarise ∘ binarise = id" $
      forAll ((,,) <$> treeGen <*> aPosInt <*> aPosInt) \(t, h, v) ->
        debinarise (binarise (Horizontal h) (Vertical v) t) == t

{- | Small test forest for this module.

   Note that this relies on the parser 'pTree' being correct, though since that
   parser is QuickChecked we are reasonably confident that it is.
-}
tForest :: PennForest
tForest = fromRight [] . parseOnly (many pTree) . BS.unlines . map (<> "\n") $
    [ "(NT (S (S (S a) (S b)) (S b)))"
    , "(NT (S (S (S a) (S b)) (S b)))"
    , "(NT (S (S (S a) (S b)) (S b)))"

    , "(NT (S (S a) (S (S b)) (S b)))"
    , "(NT (S (S a) (S (S b)) (S b)))"

    , "(NT (S (S a) (S (S a) (S b)) (S b)))"
    , "(NT (S (S a) (S (S a) (S b)) (S b)))"
    , "(NT (S (S a) (S (S a) (S b)) (S b)))"
    , "(NT (S (S a) (S (S a) (S b)) (S b)))"
    ]

{- Note [Rules]
   ~~~~~~~~~~~~~~~~~~~~~~
   With a small calculation we get the following rules:

     NT -> S     1       ~ 1
     S  -> S S   10 / 49 ~ 0.20408163265306123
     S  -> S S S  6 / 49 ~ 0.12244897959183673
     S  -> a     13 / 49 ~ 0.2653061224489796
     S  -> b     18 / 49 ~ 0.3673469387755102
     S  -> S      2 / 49 ~ 0.04081632653061224

-}
