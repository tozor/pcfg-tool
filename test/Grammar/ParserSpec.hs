{- |
   Module      : Grammar.ParserSpec
   Description : Property (and unit) tests for the treebank parser
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.ParserSpec
    ( spec  -- :: Spec
    ) where

import Core.Tree (prettyTree)
import Grammar.Parser (pTree)
import Util (treeGen)

import Test.Hspec (Spec, context, describe, it)
import Test.Hspec.Attoparsec
    ( (~>)             -- Read as "fed into".
    , parseSatisfies
    , shouldFailOn
    , shouldSucceedOn
    )
import Test.QuickCheck (forAll)


-- | Specs to export.
spec :: Spec
spec = treeSpec

{- | Spec for 'pTree'.
   Note that we have chosen to indent specs by two spaces instead of the usual
   four for readability reasons.
-}
treeSpec :: Spec
treeSpec = do
  describe "pTree" $
    context "given the empty S-expression `()`" $
      it "should fail" $
        pTree `shouldFailOn` makeSexp "()"

  describe "pTree" $
    context "given the unbalanced expression `(S t`" $
      it "should fail" $
        pTree `shouldFailOn` makeSexp "(S t"

  describe "pTree" $
    context "given the unbalanced expression `S t)`" $
      it "should fail" $
        pTree `shouldFailOn` makeSexp "S t)"

  describe "pTree" $
    context "given the expression `((S t))`" $
      it "should fail due to there being no word in the outer S-expression" $
        pTree `shouldFailOn` makeSexp "((S t))"

  -- The following three tests also test 'prettyTree' in some sense.

  describe "pTree" $
    context "given a (hardcoded) example from the treebank" $
      it "should satisfy \"prettyPrint ∘ parse = id\"" $
        treeBankEx ~> pTree `parseSatisfies` \t -> prettyTree t == treeBankEx

  describe "pTree" $
    context "given an arbitrary, valid, S-expression satisfying the following \
    \restrictions: \
    \\n      - Atoms are ASCII text.\
    \\n      - Atoms may not contain a space, nor either parenthesis \
               \character.\
    \\n      - There are no expressions of the form \"()\" or \
               \\"((S t))\"." $
      it "should parse it into a tree" $
        forAll treeGen \t -> pTree `shouldSucceedOn` prettyTree t

  describe "pTree" $
    context "given an arbitrary rose tree with ASCII leaves" $
      it "should satisfy \"parse ∘ prettyPrint = id\"" $
        forAll treeGen \t -> prettyTree t ~> pTree `parseSatisfies` (== t)
 where
  treeBankEx :: ByteString
  treeBankEx = "(ROOT (S (`` ``) (NP-SBJ (PRP I)) (VP (VBP have) (S (VP (TO to) (VP (VB come) (ADVP-CLR (RB here))))) (S-PRP (VP (TO to) (VP (VB read) (NP (DT the) (NNS walls))))) (PRN (, ,) ('' '') (SINV (VP (VBZ says)) (NP-SBJ (DT the) (NN doctor))) (, ,)) (`` ``) (SBAR-PRP (IN because) (S (NP-SBJ (PRP it)) (VP (VBZ 's) (NP-PRD (NP (NN information)) (SBAR (S (NP-SBJ (PRP I)) (ADVP-TMP (RB still)) (VP (MD ca) (RB n't) (VP (VB get) (PP (IN through) (NP (DT the) (NNS newspapers)))))))))))) (. .) ('' '')))"

  makeSexp :: ByteString -> ByteString
  makeSexp = (<> "\n")
