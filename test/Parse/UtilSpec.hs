{- |
   Module      : Parse.UtilSpec
   Description : Unit tests for functions in 'Parse.Util'
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.UtilSpec
    ( spec  -- :: Spec
    ) where

import Parse.Parser (pLexicalLeft, pNonLexical, pNonLexicalLeft)
import Parse.Types (LeftLex(LeftLex), LhsAccess(LhsAccess, nts), RhsAccess)
import Parse.Util (insideOutside)
import Test.Hspec (Spec, context, describe, it)

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as Map

import Data.Attoparsec.ByteString (parseOnly)


-- | Specs to export.
spec :: Spec
spec = insideOutsideSpec

-- | Small test for the 'insideOutside' function.
insideOutsideSpec :: Spec
insideOutsideSpec =
  describe "insideOutside" $
    context "given a small CFG" $
      it "should produce the correct results" $
        sort outs == sort
                     -- on a rainy afternoon we calculated the following
                     [ ("ROOT", 1     )
                     , ("S"   , 1     )
                     , ("N"   , 0.33  )
                     , ("PP"  , 0.33  )
                     , ("NP"  , 0.33  )
                     , ("VP"  , 0.33  )
                     , ("P"   , 0.165 )
                     , ("Det" , 0.165 )
                     , ("V"   , 0.0825)
                     ]

outs :: [(ByteString, Double)]
outs = toList $ insideOutside "ROOT" lRules' rRules lexicon
  where
    LhsAccess{ nts } = lRules
    lRules'          = lRules
        { nts = nts <> fromList [("V", 0), ("P", 0), ("N", 0), ("Det", 0)] }

rRules :: RhsAccess
rRules = mconcat
       . fromRight mempty
       . parseOnly (many pNonLexical)
       . BS.unlines
       $ rules

lRules :: LhsAccess
lRules = mconcat
       . fromRight mempty
       . parseOnly (many pNonLexicalLeft)
       . BS.unlines
       $ rules

lexicon :: LeftLex
lexicon = LeftLex
        . Map.fromListWith (<>)
        . fromRight mempty
        . parseOnly (many pLexicalLeft)
        . BS.unlines
        $ [ "VP eats 0.5"
          , "VP she 0.5"
          , "NP she 0.5"
          , "NP eats 0.5"
          , "V eats 1"
          , "P with 1"
          , "N fish 0.5"
          , "N fork 0.5"
          , "Det a 1"
          ]

rules :: [ByteString]
rules =
    [ "ROOT -> S 1"
    , "S -> NP VP 0.33"
    , "VP -> VP PP 0.5"
    , "VP -> V NP 0.5"
    , "PP -> P NP 1"
    , "NP -> Det N 1"
    , "S -> VP 0.33"
    , "S -> PP 0.33"
    ]
