{- |
   Module      : Parse.DeductiveSpec
   Description : Unit tests for functions in 'Parse.Deductive'
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.DeductiveSpec
    ( spec  -- :: Spec
    ) where

import Parse.Deductive (deduce)
import Parse.Parser (pLexical, pNonLexical)
import Parse.Queue (enlist)
import Parse.Types (LexRules, RhsAccess)
import Parse.Util (getBestTree)

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as Map

import Data.Attoparsec.ByteString (parseOnly)
import Test.Hspec (Spec, context, describe, it, shouldBe)


-- | Specs to export.
spec :: Spec
spec = deductiveSpec

{- | In lieu of good invariants, we again have to resort to unit tests.

   Note that we have chosen to indent specs by two spaces instead of the usual
   four for readability reasons.
-}
deductiveSpec :: Spec
deductiveSpec =
  describe "deduce" $
    context "given a small CFG" $
      it "should produce the correct results" $
        let (rhs, lex) = (rules, lexicon)
            deduce' = deduce (const id) enlist "S" lex rhs
         in getBestTree "S" False mempty deduce' <$> input `shouldBe` parses

{- | With some effort we calculate the following reults for the input sentences.

   (S (NP (Det a) (N fish)) (VP (VP eats) (PP (P with) (NP she))))
   (S (NP (Det a) (N fish)) (VP (VP eats) (PP (P with) (NP eats))))
   (S (NP (Det a) (N fish)) (VP (VP she) (PP (P with) (NP eats))))
   (S (NP (Det a) (N fish)) (VP (VP she) (PP (P with) (NP she))))
   (S (NP she) (VP (VP (V eats) (NP (Det a) (N fish))) (PP (P with) (NP (Det a) (N fork)))))
   (NOPARSE she eats fish a with fork a)
-}
input :: [[ByteString]]
input = map BS.words
    [ "a fish eats with she"
    , "a fish eats with eats"
    , "a fish she with eats"
    , "a fish she with she"
    , "she eats a fish with a fork"
    , "she eats fish a with fork a"
    ]

parses :: [ByteString]
parses =
    [ "(S (NP (Det a) (N fish)) (VP (VP eats) (PP (P with) (NP she))))"
    , "(S (NP (Det a) (N fish)) (VP (VP eats) (PP (P with) (NP eats))))"
    , "(S (NP (Det a) (N fish)) (VP (VP she) (PP (P with) (NP eats))))"
    , "(S (NP (Det a) (N fish)) (VP (VP she) (PP (P with) (NP she))))"
    , "(S (NP she) (VP (VP (V eats) (NP (Det a) (N fish))) (PP (P with) (NP (Det a) (N fork)))))"
    , "(NOPARSE she eats fish a with fork a)"
    ]

rules :: RhsAccess
rules = mconcat . fromRight mempty . parseOnly (many pNonLexical) . BS.unlines $
    [ "S -> NP VP 1"
    , "VP -> VP PP 0.5"
    , "VP -> V NP 0.5"
    , "PP -> P NP 1"
    , "NP -> Det N 1"
    ]

lexicon :: LexRules
lexicon = Map.fromListWith (<>)
        . fromRight mempty
        . parseOnly (many pLexical)
        . BS.unlines
        $ [ "VP eats 1"
          , "VP she 1"
          , "NP she 1"
          , "NP eats 1"
          , "V eats 1"
          , "P with 1"
          , "N fish 0.5"
          , "N fork 0.5"
          , "Det a 1"
          ]
