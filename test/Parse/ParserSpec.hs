{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

{- |
   Module      : Parser.ParserSpec
   Description : Property tests for parsing .rules and .lexicon files.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.ParserSpec
    ( spec  -- :: Spec
    ) where

import Parse.Parser (pLexical, pNonLexical)
import Parse.Types
    ( LexItem
    , NL1(NL1)
    , NL2Left(NL2Left)
    , NL2Right(NL2Right)
    , NonLexical1(NonLexical1)
    , NonLexical2(NonLexical2)
    , RhsAccess(RhsAccess)
    )
import Util ((<<>>), wordGenNoNumber)

import Test.Hspec (Spec, context, describe, it)
import Test.Hspec.Attoparsec
    ( (~>)             -- Read as "fed into".
    , parseSatisfies
    , shouldSucceedOn
    )
import Test.QuickCheck (Gen, arbitrary, forAll, listOf)


-- | Specs to export.
spec :: Spec
spec = lexSpec
    *> nonlexSpec

{- | Spec for 'pLexical'.
   Note that we have chosen to indent specs by two spaces instead of the usual
   four for readability reasons.
-}
lexSpec :: Spec
lexSpec = do
  describe "pLexical" $
    context "given a random lexical rule" $
      it "should parse it" $
        forAll lexicalGen' \r -> pLexical `shouldSucceedOn` r

  describe "pLexical" $
    context "given a nicely written lexical rule" $
      it "should satisfy \"prettyPrint ∘ parse = id\"" $
        forAll lexicalGen \r ->
          r ~> pLexical `parseSatisfies` ((r ==) . prettyLexical)

{- | Spec for 'pNonLexical'.
   Note that we have chosen to indent specs by two spaces instead of the usual
   four for readability reasons.
-}
nonlexSpec :: Spec
nonlexSpec = do
  describe "pNonLexical" $
    context "given a random unary non-lexical rule" $
      it "should parse it" $
        forAll nonLexical1Gen' \r -> pNonLexical `shouldSucceedOn` r

  describe "pNonLexical" $
    context "given a nicely written, unary, non-lexical rule" $
      it "should satisfy \"prettyPrint ∘ parse = id\"" $
        forAll nonLexical1Gen \r ->
          r ~> pNonLexical `parseSatisfies` ((r ==) . prettyNonLexical1)

  describe "pNonLexical" $
    context "given a random binary non-lexical rule" $
      it "should satisfy \"prettyPrint ∘ parse = id\"" $
        forAll nonLexical2Gen' \r -> pNonLexical `shouldSucceedOn` r

  describe "pNonLexical" $
    context "given a nicely written, binary, non-lexical rule" $
      it "should satisfy \"prettyPrint ∘ parse = id\"" $
        forAll nonLexical2Gen \r ->
          r ~> pNonLexical `parseSatisfies` ((r ==) . prettyNonLexical2)

-- *Beautiful* testing code ahead.

spaces :: Gen ByteString
spaces = fromString <$> listOf (pure ' ')

lexicalGen :: Gen ByteString
lexicalGen = ruleGen 1 " " " "

lexicalGen' :: Gen ByteString
lexicalGen' = join $
    ruleGen 1
        <$> (spaces <<>> pure " ")
        <*> (spaces <<>> pure " ")

nonLexical1Gen :: Gen ByteString
nonLexical1Gen = ruleGen 1 " -> " " "

nonLexical1Gen' :: Gen ByteString
nonLexical1Gen' = join $
    ruleGen 1
        <$> (spaces <<>> pure " -> " <<>> spaces)
        <*> (spaces <<>> pure " ")
nonLexical2Gen' :: Gen ByteString

nonLexical2Gen :: Gen ByteString
nonLexical2Gen = ruleGen 2 " -> " " "

nonLexical2Gen' = join $
    ruleGen 2
        <$> (spaces <<>> pure " -> " <<>> spaces)
        <*> (spaces <<>> pure " ")

ruleGen :: Int -> ByteString -> ByteString -> Gen ByteString
ruleGen n s s' = wordGenNoNumber
            <<>> pure s
            <<>> mconcat <$> replicateM n (wordGenNoNumber <<>> pure s')
            <<>> show <$> arbitrary @Double

-- It only gets worse from here on out.

prettyLexical :: LexItem -> ByteString
prettyLexical (t, (n, w, _) :| []) = mconcat [n, " ", t, " ", show w]

prettyNonLexical1 :: RhsAccess -> ByteString
prettyNonLexical1 (RhsAccess (NL1 nl1) _ _) =
    case toList <<$>> toList nl1 of
        [(rhs', [NonLexical1 lhs rhs w _])] ->
            if rhs' == rhs
                then mconcat [lhs, " -> ", rhs, " ", show w]
                else error "Unary invariants not fulfilled by pNonLexical"

type NL2 = HashMap ByteString (HashSet NonLexical2)
prettyNonLexical2 :: RhsAccess -> ByteString
prettyNonLexical2 (RhsAccess _ nl2left nl2right) =
    case toList <<$>> toList (un @NL2 nl2left) of
        [(rhs1'', [it1@(NonLexical2 lhs rhs1 rhs2 w _)])] ->
            case toList <<$>> toList (un @NL2 nl2right) of
                [(rhs2'', [it2])] ->
                    if rhs1'' == rhs1 && rhs2'' == rhs2 && it1 == it2
                        then mconcat
                                 [lhs, " -> ", rhs1, " ", rhs2, " ", show w]
                        else error
                                 "Bin invariants not fulfilled by pNonLexical"
