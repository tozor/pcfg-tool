# HACK: There is no locale set on the VM and Haskell's Strings don't
# particularly like that, hence help texts are broken by default (as far as we
# can tell the actual program works just fine).
# This may be fixed with the following in ~/.bashrc (or some place similar):
#
#     export LANG=en_US.UTF-8
#     export LC_ALL=en_US.UTF-8
#
# This was agreed upon with Richard, at least until the locale situation is
# fixed on the machine.  If this gets compiled on another machine we assume that
# the locale has been set properly.

# If you compile this on a new stack install with no GHC installed already, make
# yourself a nice cup of tea!

package = pcfg-tool

stack_yaml = STACK_YAML="stack.yaml"
stack = $(stack_yaml) stack

# Make sure we are on the latest version of stack (version installed on the VM
# is 1.x, that's not what we want).
update_stack = $(stack) update && $(stack) upgrade

# Force the use of the updated (local) version of stack.
lstack = $(stack_yaml) ~/.local/bin/stack

build:
	$(update_stack)
	[ -f ~/.local/bin/stack ] || ln -s $(shell command -v stack) ~/.local/bin/stack
	$(lstack) install --local-bin-path . $(package)

test:
	$(update_stack) && $(lstack) test $(package)

tests: pcfg_tool
	${MAKE} -f tests.mk

.PHONY : build test tests
