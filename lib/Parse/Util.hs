{- |
   Module      : Parse.Util
   Description : Utility functions for parsing context-free grammars.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Util
    ( -- * Looking up items in a left access map
      ItemWas(..)
    , insertLookup       -- :: (Int, ByteString) -> (Found, Weight) -> LeftAccess -> (ItemWas, LeftAccess)

      -- * Pretty printing the best parse tree
    , getBestTree        -- :: ByteString -> Bool -> LexRules -> ([ByteString] -> Backtraces) -> [ByteString] -> ByteString

      -- * Unking as reflected in the parser
    , unkSentence        -- :: LexRules -> [ByteString] -> ([ByteString], [ByteString])
    , unUnkSentence      -- :: ByteString -> [ByteString] -> ByteString

      -- * Inside-Outside algorithm for A*-parsing
    , prettyOuts         -- :: OutWeights -> ByteString
    , insideOutside      -- :: ByteString -> LhsAccess -> RhsAccess -> LeftLex -> OutWeights
    , getOutsideWeights  -- :: RhsAccess -> LexRules -> OutWeights -> (RhsAccess, LexRules)
    ) where

import Core.Tree (PennTree, Tree(Node, Preterminal), prettyTree)
import Core.Util (Weight, mconcatMap)
import Parse.Types
    ( Backtrace(Bin, Chain, Term)
    , Backtraces
    , Found(Item)
    , LeftAccess
    , LeftLex(LeftLex)
    , LexRules
    , Lexical(Lexical, lhs, rhs, w)
    , LhsAccess(LhsAccess, nl1, nl2, nts)
    , NL1(NL1)
    , NL2(NL2)
    , NL2Left(NL2Left)
    , NL2Right(NL2Right)
    , NonLexical1(NonLexical1, lhs, outW, rhs, w)
    , NonLexical2(NonLexical2, lhs, outW, rhs1, rhs2, w)
    , OutWeights
    , RhsAccess(RhsAccess, nl1, nl2left, nl2right)
    , WeightMap
    )

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as Map
import qualified Data.HashSet          as Set
import qualified Data.Set              as OrdSet

import Data.Double.Conversion.ByteString (toShortest)
import Data.HashMap.Strict ((!?))


-- | An inserted item may be either new (meaning it was actually inserted), or
-- known (meaning there was an item with a higher weight already in the map).
data ItemWas = Known | New

-- | Perform a lookup and an insert into the 'LeftAccess' map at the same time.
insertLookup
    :: (Int, ByteString)      -- ^ Key to look up
    -> (Found, Weight)        -- ^ Found item with associated weight
    -> LeftAccess
    -> (ItemWas, LeftAccess)
insertLookup k (item, w) = Map.alterF notifyIfNew k
  where
    -- | If the item was chosen before, ignore the new value.
    notifyIfNew :: Maybe WeightMap -> (ItemWas, Maybe WeightMap)
    notifyIfNew = fmap Just . \case
        Nothing  -> (New, one (item, w))
        Just kvs -> if   item `Map.member` kvs
                    then (Known, kvs)
                    else (New  , Map.insert item w kvs)

-- | Find the best (i.e. the one with the highest weight) parse tree.  Also
-- apply trivial unking if requested.
getBestTree
    :: ByteString                    -- ^ Initial non-terminal
    -> Bool                          -- ^ Whether to apply trivial unking
    -> LexRules                      -- ^ Lexical rules (needed only for unking)
    -> ([ByteString] -> Backtraces)  -- ^ Backtraces for input
    -> [ByteString]                  -- ^ Input sentence
    -> ByteString                    -- ^ Pretty-printed parse tree
getBestTree initNT unk lex getBts input
    | unk       = uncurry unUnkSentence . first getTree $ unkSentence lex input
    | otherwise = getTree input
  where
    getTree :: [ByteString] -> ByteString
    getTree sentence = prettyTree $ bestTree sentence (getBts sentence) initNT

-- | Get the best parse tree from the given backtraces.
bestTree
    :: [ByteString]  -- ^ Input sentence
    -> Backtraces    -- ^ Backtraces generated based upon that input
    -> ByteString    -- ^ Initial non-terminal
    -> PennTree      -- ^ Best parse tree or __NOPARSE__
bestTree input backtraces initNT = go initNT (0, length input)
  where
    go :: ByteString -> (Int, Int) -> PennTree
    go nt (start, end) = case backtraces !? Item nt start end of
        Nothing -> Preterminal "NOPARSE" (BS.unwords input)
        Just bt -> case bt of
            Term  Lexical{ lhs, rhs }                  -> Preterminal lhs rhs
            Chain NonLexical1{ lhs, rhs } i j          -> Node lhs [go rhs (i, j)]
            Bin   NonLexical2{ lhs, rhs1, rhs2 } i j k ->
                Node lhs [go rhs1 (i, j), go rhs2 (j, k)]

-- | Replace unknown words with __UNK__ in a given sentence.
unkSentence
    :: LexRules                      -- ^ Lexical rules with all known words
    -> [ByteString]                  -- ^ Tokenized input sentence
    -> ([ByteString], [ByteString])  -- ^ (unked sentence, words that were unked)
unkSentence lex = foldr
    (\w (ws, unks) ->
         if w `Map.member` lex then (w : ws, unks) else ("UNK" : ws, w : unks))
    ([], [])

-- | Restore the original leaves after parsing, i.e. replace __UNK__ with the
-- original word in the sentence.
unUnkSentence
    :: ByteString    -- ^ Pretty printed parse tree
    -> [ByteString]  -- ^ Words that were unked
    -> ByteString
unUnkSentence = foldl' go
  where
    go :: ByteString -> ByteString -> ByteString
    go bs node = if BS.null after then before else combine  -- beautiful
      where
        (before, after) = BS.drop 3 <$> BS.breakSubstring "UNK" bs

        combine :: ByteString
        combine = mconcat [before, node, after]

-- | Pretty print outside weights.
prettyOuts :: OutWeights -> ByteString
prettyOuts = mconcatMap (prettyTuple . fmap toShortest) . toList
  where
    -- | Pretty print a pair consisting of a non-terminal and its associated
    -- weight, already converted to 'ByteString'.
    prettyTuple :: (ByteString, ByteString) -> ByteString
    prettyTuple (bs, w) = mconcat [bs, " ", w, "\n"]

-- | Calculate the outside weights of all non-terminals using a modified version
-- of the inside-outside algorithm.
insideOutside :: ByteString -> LhsAccess -> RhsAccess -> LeftLex -> OutWeights
insideOutside initNT
              LhsAccess{ nl1 = nl1left, nl2, nts }
              RhsAccess{ nl1, nl2left, nl2right }
              ll
    = step initOut
  where
    ins :: HashMap ByteString Weight
    ins = inside nts ll nl2 nl1left

    -- | All non-terminals, except for the initial non-terminal, start with
    -- weight 0.
    initOut :: OutWeights
    initOut = Map.insert initNT 1 nts

    -- | Iterate over the outside weights until we have convergence.
    step :: OutWeights -> OutWeights
    step outs = if outs == outs' then outs else step outs'
      where
        outs' :: OutWeights
        outs' = Map.mapWithKey
            (\ntB outB -> outB
                    `max` leftMax  ntB
                    `max` rightMax ntB
                    `max` unaryMax ntB)
            outs

        unaryMax :: ByteString -> Weight
        unaryMax ntB = maybe 0 (maxHashSet1 lhs outs) (un nl1 !? ntB)

        leftMax :: ByteString -> Weight
        leftMax ntB = maybe 0 (maxHashSet2 lhs outs rhs2 ins) (un nl2left !? ntB)

        rightMax :: ByteString -> Weight
        rightMax ntB = maybe 0 (maxHashSet2 lhs outs rhs1 ins) (un nl2right !? ntB)

-- | Calculate the inside weights for the inside-outside algorithm.
inside
    :: HashMap ByteString Weight  -- ^ All non-terminals, associated with weight 0
    -> LeftLex                    -- ^ Access to A in @A b w@
    -> NL2                        -- ^ Access to A in @A ⟶ B C w@
    -> NL1                        -- ^ Access to A in @A ⟶ B w@
    -> HashMap ByteString Weight
inside nts (LeftLex la) (NL2 nl2) (NL1 nl1) = step initWeights
  where
    -- | Initial weights, i.e. the non-terminal A gets the maximal weight of all
    -- rules of the form @A ⟶ t@ (where t is some terminal symbol).
    initWeights :: HashMap ByteString Weight
    initWeights = Map.map maxWeightRule la <> nts

    -- | Iterate over the inside weights until we have convergence.
    step :: HashMap ByteString Weight -> HashMap ByteString Weight
    step ins = if ins == ins' then ins else step ins'
      where
        ins' :: HashMap ByteString Weight
        ins' = Map.mapWithKey
            (\ntA inA ->
                 inA `max` maxHashSet2 rhs1 ins rhs2 ins (lookupMempty ntA nl2)
                     `max` maxHashSet1 rhs ins (lookupMempty ntA nl1))
            ins

        lookupMempty :: (Eq k, Hashable k, Monoid v) => k -> HashMap k v -> v
        lookupMempty = Map.lookupDefault mempty

-- | Get the element with the maximal weight in a given 'Set'.
maxWeightRule :: Set Lexical -> Weight
maxWeightRule =
    fromMaybe 0 . OrdSet.lookupMax . OrdSet.map (w :: Lexical -> Weight)

maxHashSet1
    :: (NonLexical1 -> ByteString)  -- ^ Get the weight of this non-terminal...
    -> HashMap ByteString Weight    -- ^ ... out of this 'HashMap'
    -> HashSet NonLexical1          -- ^ 'HashSet' to fold over
    -> Weight
maxHashSet1 nt hm =
    foldl' (\a rule@NonLexical1{ w } -> a `max` (lookup0 (nt rule) hm * w)) 0

maxHashSet2
    :: (NonLexical2 -> ByteString)  -- ^ Get the weight of this non-terminal...
    -> HashMap ByteString Weight    -- ^ ... out of this 'HashMap'
    -> (NonLexical2 -> ByteString)  -- ^ Get the weight of this non-terminal...
    -> HashMap ByteString Weight    -- ^ ... out of this 'HashMap'
    -> HashSet NonLexical2          -- ^ 'HashSet' to fold over
    -> Weight
maxHashSet2 nt hm nt' hm' = foldl'
    (\a rule@NonLexical2{ w } ->
         a `max` (lookup0 (nt rule) hm * w * lookup0 (nt' rule) hm'))
    0

-- | Look up a numerical key in a 'HashMap'.
lookup0 :: (Eq k, Hashable k, Num v) => k -> HashMap k v -> v
lookup0 = Map.lookupDefault 0

{- | Add the outside weights to the respective access maps.  This is useful when
   parsing a sentence, as we quickly need to refer to this weight for an
   enqueued item.
-}
getOutsideWeights :: RhsAccess -> LexRules -> OutWeights -> (RhsAccess, LexRules)
getOutsideWeights RhsAccess{ nl1, nl2left, nl2right } lex outs =
    (RhsAccess nl1' nl2left' nl2right', lex')
  where
    nl2left'  = getOuts2 `under` nl2left
    nl2right' = getOuts2 `under` nl2right
    nl1'      = getOuts1 `under` nl1
    lex'      = getOutsLex lex

    getOuts2 :: HashMap k (HashSet NonLexical2) -> HashMap k (HashSet NonLexical2)
    getOuts2 = Map.map
        (Set.map \r@NonLexical2{ lhs } ->
                     r { outW = Map.lookupDefault 1 lhs outs } :: NonLexical2)

    getOuts1 :: HashMap k (HashSet NonLexical1) -> HashMap k (HashSet NonLexical1)
    getOuts1 = Map.map
        (Set.map \r@NonLexical1{ lhs } ->
                     r { outW = Map.lookupDefault 1 lhs outs } :: NonLexical1)

    getOutsLex :: LexRules -> LexRules
    getOutsLex =
        Map.map (fmap \(lhs, w, _) -> (lhs, w, Map.lookupDefault 1 lhs outs))
