{- |
   Module      : Parse.Outside
   Description : Interface for the /outside/ command.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Outside
    ( runOutside  -- :: ByteString -> FilePath -> FilePath -> Maybe FilePath -> IO ()
    ) where

import Core.Util (streamParse)
import Parse.Parser (pLexicalLeft, pNonLexical, pNonLexicalLeft)
import Parse.Types (LeftLex(LeftLex), LhsAccess(LhsAccess, nts), RhsAccess)
import Parse.Util (insideOutside, prettyOuts)

import qualified Data.ByteString.Streaming.Char8 as SB
import qualified Data.HashMap.Strict             as Map
import qualified Streaming.Prelude               as S

import Control.Monad.Trans.Resource (runResourceT)


-- | 'IO' interface for the /outside/ command.
runOutside :: ByteString -> FilePath -> FilePath -> Maybe FilePath -> IO ()
runOutside initNT rules lexicon mbFile = do
    (lhs@LhsAccess{ nts }, rhs, leftlex@(LeftLex ll)) <-
        (,,) <$> pStreamNonLexLeft rules
             <*> pStreamNonLex     rules
             <*> pStreamLexLeft    lexicon

    -- Actually calculate outside weights.
    let lhs' = lhs { nts = nts <> fromList [(nt, 0) | (nt, _) <- toList ll] }
        outs = prettyOuts $ insideOutside initNT lhs' rhs leftlex

    -- Depending on the users preference, print the outside weights to either
    -- stdout or a given file.
    maybe (putBS outs) (`writeFileBS` outs) mbFile

-- | Parse a /.lexicon/ file line-wise, left access.
pStreamNonLexLeft :: FilePath -> IO LhsAccess
pStreamNonLexLeft file =
    runResourceT $ streamParse (SB.readFile file) pNonLexicalLeft S.mconcat_

-- | Parse a /.lexicon/ file line-wise, right access.
pStreamNonLex :: FilePath -> IO RhsAccess
pStreamNonLex file =
    runResourceT $ streamParse (SB.readFile file) pNonLexical S.mconcat_

-- | Parse a /.grammar/ file line-wise, left access.
pStreamLexLeft :: FilePath -> IO LeftLex
pStreamLexLeft file = runResourceT $
    streamParse (SB.readFile file)
                pLexicalLeft
                (S.toList_ >>> fmap (LeftLex . Map.fromListWith (<>)))
