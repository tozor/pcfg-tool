{- |
   Module      : Parse.Command
   Description : Interface for the /parse/ command.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Command
    ( runParse  -- :: ParseOptions -> FilePath -> FilePath -> IO ()
    ) where

import Core.CLI
    ( ParseOptions(ParseOptions, astar, initNT, rkBeam, thBeam, unking)
    )
import Core.Util (parMapN, streamParse)
import Parse.Deductive (deduce)
import Parse.Parser (pLexical, pNonLexical, pOutside)
import Parse.Queue (Queue, QueueItem, enlist, enlistFixed, thPrune)
import Parse.Types (LexRules, OutWeights, RhsAccess)
import Parse.Util (getBestTree, getOutsideWeights)

import qualified Data.ByteString.Char8           as BS
import qualified Data.ByteString.Streaming.Char8 as SB
import qualified Data.HashMap.Strict             as Map
import qualified Streaming.Prelude               as S

import Control.Monad.Trans.Resource (runResourceT)
import Control.Parallel.Strategies (rdeepseq)


-- | 'IO' interface for the /parse/ command.
runParse :: ParseOptions -> FilePath -> FilePath -> IO ()
runParse ParseOptions{ initNT, unking, thBeam, rkBeam, astar } nonlexF lexF = do
    (rhs, lex) <- (,) <$> pStreamNonLex nonlexF <*> pStreamLex lexF

    -- Possibly add outside weights and pruning.
    outs <- maybe (pure mempty) pStreamOutside astar
    let (rhs', lex') = getOutsideWeights rhs lex outs
        deduce'      = deduce thFun enqueueWith initNT lex' rhs'

    -- Asynchronously handle the input sentences, see Note [Stream Input].
    input <- BS.lines <$> BS.getContents
    let sentences = parMapN
            (length input `div` 10)
            rdeepseq
            (getBestTree initNT unking lex deduce' . BS.words)
            input

    -- Output sentences in same order as they were input.
    traverse_ putBSLn sentences
  where
    thFun :: Int -> Queue -> Queue
    thFun r = if r == 0 then maybe id thPrune thBeam else id

    -- | When the queue has a fixed size, we use another enqueue function in
    -- order to combine enqueueing and pruning items.
    enqueueWith :: Queue -> [QueueItem] -> Queue
    enqueueWith = maybe enlist enlistFixed rkBeam

{- Note [Stream Input]
   ~~~~~~~~~~~~~~~~~~~~~~
   This processes the input in parallel, while still splitting up the list into
   chunks (see the haddocks for 'parMapN').  Here we use at most 10 chunks per
   list.
-}

-- | Parse a /.lexicon/ file line-wise.
pStreamNonLex :: FilePath -> IO RhsAccess
pStreamNonLex file =
    runResourceT $ streamParse (SB.readFile file) pNonLexical S.mconcat_

-- | Parse a /.grammar/ file line-wise.
pStreamLex :: FilePath -> IO LexRules
pStreamLex file = runResourceT $
    streamParse (SB.readFile file)
                pLexical
                (S.toList_ >>> fmap (Map.fromListWith (<>)))

-- | Parse a /.outside/ file line-wise.
pStreamOutside :: FilePath -> IO OutWeights
pStreamOutside file = runResourceT $
    streamParse (SB.readFile file) pOutside (S.toList_ >>> fmap fromList)
