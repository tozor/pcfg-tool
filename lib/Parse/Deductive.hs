{- |
   Module      : Parse.Deductive
   Description : Deductive parser based on Knuth's algorithm.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Deductive
    ( deduce  -- :: (Int -> Queue -> Queue) -> (Queue -> [QueueItem] -> Queue) -> ByteString -> LexRules -> RhsAccess -> [ByteString] -> Backtraces
    ) where

import Core.Util (Weight, runStream)
import Parse.Queue
    ( Queue
    , QueueItem(QueueItem)
    , emptyQueue
    , nullQueue
    , unsafeDequeue
    )
import Parse.Types
    ( Backtrace(Bin, Chain, Term)
    , Backtraces
    , Found(Item, left, right)
    , LeftAccess
    , LexRules
    , Lexical(Lexical)
    , NL1(NL1)
    , NL2Left(NL2Left)
    , NL2Right(NL2Right)
    , NonLexical1(NonLexical1, lhs, outW, w)
    , NonLexical2(NonLexical2, lhs, outW, rhs1, rhs2, w)
    , RhsAccess(RhsAccess)
    , RightAccess
    , WeightMap
    )
import Parse.Util (ItemWas(Known, New), insertLookup)

import qualified Data.HashMap.Strict as Map
import qualified Streaming.Prelude   as S

import Data.HashMap.Strict ((!?))
import Streaming.Prelude (Of, Stream)


-- We decided to use the deductive algorithm here, mainly because it allows for
-- a more idiomatic approach to the problem of parsing a context-free grammar.

-- | Run the deduction algorithm, based on Knuth's generalization of Dijkstra's
-- shortest path algorithm.
deduce
    :: (Int -> Queue -> Queue)          -- ^ Function for threshold pruning
    -> (Queue -> [QueueItem] -> Queue)  -- ^ Possible fixed-size enqueueing
    -> ByteString                       -- ^ Initial non-terminal
    -> LexRules                         -- ^ Lexical Rules
    -> RhsAccess                        -- ^ Access to rhs' of non-lexical rules
    -> [ByteString]                     -- ^ Tokenized sentence
    -> Backtraces
deduce thFun enq initNT lexicals (RhsAccess nl1 nl2left nl2right) input =
    step 100 initials mempty mempty mempty
  where
    -- | Initial queue with lexical rules.
    initials :: Queue
    initials = lexQueue enq lexicals input

    -- | A single step in the algorithm, i.e. a single traversal of the while
    -- loop (lines 4-11).  See Note [Lines].
    step :: Int -> Queue -> LeftAccess -> RightAccess -> Backtraces -> Backtraces
    step r queue accessLeft accessRight !backtraces
        | nullQueue queue = backtraces
        | otherwise       = case itemWas of
            Known -> step r' queue' accessLeft  accessRight  backtraces
            New   -> step r' queue5 accessLeft' accessRight' backtraces'
              where
                -- | Keep left and right access maps synced.
                accessRight' :: RightAccess
                accessRight' =
                    Map.insertWith (<>) (ntA, j) (one (argmax, w)) accessRight

                -- | Save the backtrace of the found item for later
                -- reconstruction of the best derivation.
                backtraces' :: Backtraces
                backtraces' = Map.insert argmax bt backtraces

                -- | Update queue with all binary rules of the form @A' ⟶ A C@.
                queue2 :: Queue
                queue2 = insLeft enq (argmax, w) queue' accessLeft' nl2left

                -- | Update queue with all binary rules of the form @A' ⟶ B A@.
                queue3 :: Queue
                queue3 = insRight enq (argmax, w) queue2 accessRight' nl2right

                -- | Update queue with all unary rules of the form @A' ⟶ A@.
                queue4 :: Queue
                queue4 = insUnary enq (argmax, w) queue3 nl1

                -- | Prune the queue if requested and check for optimal initial
                -- non-terminal, see Note [Init NT] and Note [Threshold Pruning]
                queue5 :: Queue
                queue5 = thFun r' $
                    if argmax == Item initNT 0 n then emptyQueue else queue4
                  where n = length input

      where
        -- | Item @(out(A), A, i, j, bt, w)@ with highest weight w and queue
        -- without it.  See Note [Queue].
        (argmax@(Item ntA i j), w, bt, queue') = unsafeDequeue queue

        -- | Whether the item was new.  See Note [Queue].
        (itemWas, accessLeft') = insertLookup (i, ntA) (argmax, w) accessLeft

        -- | Only prune every N rounds, see Note [Threshold Pruning].
        r' :: Int
        r' = if r == 0 then 100 else r - 1

{- Note [Queue]
   ~~~~~~~~~~~~~~~~~~~~~~
   Both (!) of the outer tuples need to be lazy, so that we are able to defer
   computation of the current maximal element until we know for sure that the
   queue is non-empty.  Hence, this is safe with regards to the queue being
   non-empty (which is the only way in which 'unsafeDequeue' may throw an
   error).
-}

{- Note [Init NT]
   ~~~~~~~~~~~~~~~~~~~~~~
   We only need the biggest weight for the initial non-terminal here, i.e. first
   Item, (0, S, |input|), where S is the initial non-terminal.  This is because
   the weight of all later such items will be lower (by Knuth's algorithm),
   hence we can ignore all queue elements with lower weights than this "optimal"
   item.
-}

{- Note [Threshold Pruning]
   ~~~~~~~~~~~~~~~~~~~~~~
   Due to the global queue of the deductive approach, very small threshold beam
   values (to the point where barely any items get pruned) are required for
   pruning to not have a very negative impact on the accuracy of the results if
   we prune every step.

   Hence, we only prune every couple of steps, in order to ensure that a
   reasonable number (i.e. neither "all of the queue", nor "only 1 item") of
   queue items are pruned.
-}

-- | Construct, based on the input sentence, the initial queue from all lexical
-- rules.  Corresponds to line 2 in the algorithm.  See Note [Lines].
lexQueue :: (Queue -> [QueueItem] -> Queue) -> LexRules -> [ByteString] -> Queue
lexQueue enqueue lexicals = enqueue emptyQueue . go 1 []
  where
    go :: Int           -- ^ i, such that (i - 1, i) is the current span
       -> [QueueItem]
       -> [ByteString]  -- ^ Tokenized sentence
       -> [QueueItem]
    go _ items []     = items
    go i items (t:ts) = case lexicals !? t of
        Nothing    -> mempty  -- Shortcut for unknown words.
        Just rules -> go (i + 1) (newItems ++ items) ts
                      -- Only walk over the new part of the list.
          where
            newItems :: [QueueItem]
            newItems = foldl'
                (\its (ntA, w, outW) ->
                     QueueItem (w * outW)
                               (Item ntA (i - 1) i)
                               w
                               (Term $ Lexical ntA t w)
                     : its)
                []
                rules

-- | For a found item A, insert all rules of the form @A' ⟶ A C@ into the queue.
-- Corresponds to line 9 in the algorithm.  See Note [Lines] and Note [Stream].
insLeft
    :: (Queue -> [QueueItem] -> Queue)
    -> (Found, Weight)  -- ^ Found Item: @(A, i, j, q)@
    -> Queue            -- ^ Items waiting to be found
    -> LeftAccess       -- ^ Access to found items from the left
    -> NL2Left          -- ^ Rules indexed by the first rhs non-terminal
    -> Queue
insLeft enqueue (Item ntA i j, q) queue leftAccess (NL2Left nl2Left) =
    runStream $ S.fold_ enqueue queue id (S.map itemsForRule rules)
  where
    -- | All rules of the form @A' ⟶ A C@, where A is our found non-terminal.
    rules :: Stream (Of NonLexical2) Identity ()
    rules = maybe mempty S.each (nl2Left !? ntA)

    -- | Given a rule @A' ⟶ A C@, construct all possible queue items for it.
    itemsForRule :: NonLexical2 -> [QueueItem]
    itemsForRule rule@NonLexical2{ lhs = ntA', rhs2 = ntC, w, outW } =
        maybe mempty (getItemsWith newItem posSpan) (leftAccess !? (j, ntC))
      where
        -- | Build a new item.  @w' = c_{j, j', C}@.
        newItem :: Found -> Weight -> QueueItem
        newItem Item{ right = j' } w' =
            QueueItem (newW * outW) (Item ntA' i j') newW (Bin rule i j j')
              where newW = w * q * w'

        -- | Make sure that we don't have any non-positive spans.
        posSpan :: WeightMap -> WeightMap
        posSpan = Map.filterWithKey \Item{ right = j' } _ -> j < j'

-- | For a found item A, insert all rules of the form @A' ⟶ B A@ into the queue.
-- Corresponds to line 10 in the algorithm.  See Note [Lines] and Note [Stream].
insRight
    :: (Queue -> [QueueItem] -> Queue)
    -> (Found, Weight)  -- ^ Found Item: @(A, i, j, q)@
    -> Queue            -- ^ Items waiting to be found
    -> RightAccess      -- ^ Access to found items from the right
    -> NL2Right         -- ^ Rules indexed by the second rhs non-terminal
    -> Queue
insRight enqueue (Item ntA i j, q) queue rightAccess (NL2Right nl2right) =
    runStream $ S.fold_ enqueue queue id (S.map itemsForRule rules)
  where
    -- | All rules of the form @A' ⟶ B A@, where A is our found non-terminal.
    rules :: Stream (Of NonLexical2) Identity ()
    rules = maybe mempty S.each (nl2right !? ntA)

    -- | Given a rule @A' ⟶ B A@, construct all possible queue items for it.
    itemsForRule :: NonLexical2 -> [QueueItem]
    itemsForRule rule@NonLexical2{ lhs = ntA', rhs1 = ntB, w, outW } =
        maybe mempty (getItemsWith newItem posSpan) (rightAccess !? (ntB, i))
      where
        -- | Build a new item.  @w' = c_{i', i, B}@.
        newItem :: Found -> Weight -> QueueItem
        newItem Item{ left = i' } w' =
            QueueItem (newW * outW) (Item ntA' i' j) newW (Bin rule i' i j)
              where newW = w * w' * q

        -- | Make sure that we don't have any non-positive spans.
        posSpan :: WeightMap -> WeightMap
        posSpan = Map.filterWithKey \Item{ left = i' } _ -> i' < i

{- | For a rule (which is not explicitly given as an argument, but rather
   encoded via 'makeItem' and 'prepareMap'), generated all items that fit the
   conditions applied in 'makeItem', after the queue has been changed
   (i.e. filtered) in some way with 'prepareMap'.
-}
getItemsWith
    :: (Found -> Weight -> QueueItem)  -- ^ Constructing an item
    -> (WeightMap -> WeightMap)        -- ^ Filtering the map beforehand
    -> WeightMap
    -> [QueueItem]
getItemsWith makeItem prepareMap =
    Map.foldlWithKey' (\its item w -> makeItem item w : its) [] . prepareMap

-- | For a found item A, insert all rules of the form @A' ⟶ A@ into the queue.
-- Corresponds to line 11 in the algorithm.  See Note [Lines] and Note [Stream].
insUnary
    :: (Queue -> [QueueItem] -> Queue)
    -> (Found, Weight)  -- ^ Found Item: @(A, i, j, q)@
    -> Queue            -- ^ Items waiting to be found
    -> NL1              -- ^ Rules of the Form @A' ⟶ A@
    -> Queue
insUnary enqueue (Item ntA i j, q) queue (NL1 nl1) =
    enqueue queue $ case nl1 !? ntA of
        Nothing    -> mempty
        Just rules -> runStream
                    $ S.each rules
                    & S.foldMap_ \rule@NonLexical1{ lhs = ntA', w, outW } ->
                                     one $ QueueItem (w * q * outW)
                                                     (Item ntA' i j)
                                                     (w * q)
                                                     (Chain rule i j)

{- Note [Lines]
   ~~~~~~~~~~~~~~~~~~~~~~
   Functions corresponding to specific (nontrivial) lines in the algorithm have
   been marked accordingly.

   We chose a very similar naming scheme to the algorithm for these functions,
   mainly so they are easier to read for someone already familiar with the
   theoretical side of things.  This means that i and i' will always refer to
   the left side of a span, while j and j' will refer to the right side.  For
   weights we will use w, w', and q (where w' will mostly be used to refer to
   the weight c_{i, j, A} for a non-terminal A).  Two exceptions to this rule
   have been made:

       1. We generally prefer to use w instead of q where possible, such that
          one does not accidentally read it as "queue".
       2. In lexical rules, we decided to use t instead of omega for terminal
          symbols.

   Capital letters were, due to variables starting with capital letters having a
   certain syntactic meaning in Haskell, replaced by 'ntCAPITAL' (the mnemonics
   for which are "non-terminal *letter*").
-}

{- Note [Stream]
   ~~~~~~~~~~~~~~~~~~~~~~
   Streaming the update operations is a bit faster (and slightly more memory
   efficient) when the queue gets very large (as is expected) and about as fast
   as a normal 'foldMap' otherwise.

   Because streams always have to live inside some kind of monad, they need to
   be run at the end.  However, because what we are doing here does not actually
   require any monadic computation, and thus we are dealing with pure streams,
   this is just the 'Identity' monad in our case.

   As before, we prefer to write streams in covariant notation whenever
   possible.
-}
