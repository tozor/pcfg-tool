{-# LANGUAGE StrictData #-}

{- |
   Module      : Parse.Queue
   Description : Type and utility functions for a queue of found items.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Queue
    ( -- * Queue type
      Queue          -- abstract
    , QueueItem(..)

      -- * Constructing queues
    , emptyQueue     -- :: Queue
    , enlist         -- :: Queue -> [QueueItem] -> Queue
    , enlistFixed    -- :: Int -> Queue -> [QueueItem] -> Queue

      -- * Destructing queues
    , unsafeDequeue  -- :: Queue -> (Found, Weight, Backtrace, Queue)

      -- * Utility functions
    , nullQueue      -- :: Queue -> Bool
    , thPrune        -- :: Weight -> Queue -> Queue
    ) where

import Core.Util (Weight)
import Parse.Types (Backtrace, Found)

import qualified Data.HashMap.Strict as Map
import qualified Data.Map.Strict     as OrdMap

import Data.HashMap.Strict ((!?))
import Relude.Extra.Lens (Lens', (%~), (.~), lens)


{- | Queue type.  For a queue it is rather important that the items are ordered,
   hence this uses a 'Map' instead of a 'HashMap'.

   Queue's only need to contain the same found item twice if the second instance
   has a higher weight than the first one had.  Hence, we keep a history of
   enqueued items, together with their weight, so as to not enqueue items
   unnecessarily.

   A queue may never associate a key with an empty value (in this case the key
   should have been deleted from the queue).  We use 'NonEmpty' here, in order
   to guarantee this at the type level.
-}
data Queue = Queue
    { _seen  :: HashMap Found Weight
      -- ^ History of previously enqueued items and their associated weights.
    , _enqItems :: Map Weight (NonEmpty QI)
      -- ^ Currently enqueued items.
    }

-- | Convenience strict type for a currently enqueued item.  Internal.
data QI = QI Found Backtrace Weight

-- In the following, we will use lenses in order to make record updates a bit
-- more idiomatic.

-- | Lens for the history of enqueued items.
seen :: Lens' Queue (HashMap Found Weight)
seen = lens getter setter
  where getter     = _seen
        setter q s = q { _seen = s }

-- | Lens for the currently enqueued items.
enqItems :: Lens' Queue (Map Weight (NonEmpty QI))
enqItems = lens getter setter
  where getter       = _enqItems
        setter q its = q { _enqItems = its }

-- | Corresponds to a queue item in the following way:
-- @QueueItem w it w' bt <--> Map(w, it bt w')@
data QueueItem = QueueItem Weight Found Weight Backtrace

-- | A fresh queue.
emptyQueue :: Queue
emptyQueue = Queue mempty mempty

-- | Enqueue a list of items.  We have to resort to this in lieu of having an
-- efficient implementation of '(<>)' for the 'Queue' type.
enlist :: Queue -> [QueueItem] -> Queue
enlist = foldl' enqueue

-- | Same as 'enlist', only respect the fact that the queue has a max size.
enlistFixed :: Int -> Queue -> [QueueItem] -> Queue
enlistFixed = foldl' . enqueueFixed

-- | Insert a key-value pair into a queue.  Returns the input queue if the item
-- had previously been enqueued with a higher weight.
enqueue :: Queue -> QueueItem -> Queue
enqueue queue@Queue{ _seen } (QueueItem outW item w bt) =
    maybe enqueueItem
          (\w' -> if outW > w' then enqueueItem else queue)
          (_seen !? item)
  where
    enqueueItem :: Queue
    enqueueItem = queue
        & seen     %~ Map.insert item outW
        & enqItems %~ OrdMap.insertWith (<>) outW (one (QI item bt w))

-- | Same as 'enqueue', only respect the fact that the queue has a max size.
enqueueFixed :: Int -> Queue -> QueueItem -> Queue
enqueueFixed fixedBeam queue@Queue{_seen, _enqItems} (QueueItem outW item w bt) =
    maybe go (\w' -> if outW > w' then go else queue) (_seen !? item)
  where
    go :: Queue
    go | OrdMap.size _enqItems < fixedBeam = enqueueItem
       | otherwise = case OrdMap.minViewWithKey _enqItems of
           Nothing                      -> queue  -- this can never happen
           Just ((minW, _ :| xs), enqs) -> if minW > outW
               then queue
               else queue
                   & seen     %~ Map.insert item outW
                   & enqItems .~ tryEnq
                         (OrdMap.insertWith (<>) outW (one (QI item bt w)) enqs)
             where
               -- | If the 'xs' wasn't the empty list we need to re-insert it
               -- back into the queue.
               tryEnq :: Map Weight (NonEmpty QI) -> Map Weight (NonEmpty QI)
               tryEnq = maybe id (OrdMap.insertWith (<>) minW) (nonEmpty xs)

    enqueueItem :: Queue
    enqueueItem = queue
        & seen     %~ Map.insert item outW
        & enqItems %~ OrdMap.insertWith (<>) outW (one $ QI item bt w)

{- | Get the item with the highest weight from the queue.  Because weights need
   not be unique, this function may need to re-insert parts of the found item
   back into the queue.

   NOTE: Will throw an exception if the input queue is empty.
-}
unsafeDequeue :: Queue -> (Found, Weight, Backtrace, Queue)
unsafeDequeue queue@Queue{ _enqItems } = (item, w, bt, ) $!
    queue & enqItems .~ maybe enqs
                              (\vs' -> OrdMap.insert outW vs' enqs)
                              (nonEmpty vs)
  where
    ((outW, (QI item bt w) :| vs), enqs) = OrdMap.deleteFindMax _enqItems

-- | A queue is empty if it has no enqueued items.
nullQueue :: Queue -> Bool
nullQueue = null . _enqItems

{- | Pruning via a threshold beam.

   We note that this will take the outside weights of the items into account
   when A*-parsing is enabled.  This is due to the way the queue is structured
   in that case (i.e. in '_enqItems', we keep track of the outside weights by
   maintaining the mapping @p(A -> α) · out(A) -> [item A -> α]@).
-}
thPrune :: Weight -> Queue -> Queue
thPrune thresh queue@Queue{ _enqItems } = case OrdMap.lookupMax _enqItems of
    Nothing     -> queue
    Just (m, _) ->
        queue & enqItems %~ OrdMap.filterWithKey (const . (> thresh * m))
