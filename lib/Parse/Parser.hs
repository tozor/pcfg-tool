{- |
   Module      : Parse.Parser
   Description : Parser for (binarised) .rules, .lexicon, and .outside files.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Parser
    ( pLexical         -- :: Parser LexItem
    , pLexicalLeft     -- :: Parser (ByteString, Set Lexical)
    , pNonLexical      -- :: Parser RhsAccess
    , pNonLexicalLeft  -- :: Parser LhsAccess
    , pOutside         -- :: Parser (ByteString, Double)
    ) where

import Parse.Types
    ( LexItem
    , Lexical(Lexical)
    , LhsAccess(LhsAccess)
    , NonLexical1(NonLexical1, lhs, rhs)
    , NonLexical2(NonLexical2, lhs, rhs1, rhs2)
    , RhsAccess(RhsAccess)
    )

import Data.Attoparsec.ByteString (Parser)
import Data.Attoparsec.ByteString.Char8
    ( double
    , endOfInput
    , endOfLine
    , skipSpace
    , takeWhile1
    )


-- | Parse a single consisting of a lexical item of the form @A b w@ into some
-- structure indexed by the right hand side (i.e. the b).
pLexical :: Parser LexItem
pLexical = skipSpace *> go <* end
  where
    -- | Parse @A b w@ into the right-indexed form @(b, [(A, w, w')])@.  The w'
    -- will be the outside weight of the rule, hence it is set to 0 for now.
    go :: Parser LexItem
    go = do
        lhs <- takeSkipSpace1
        rhs <- takeSkipSpace1
        (rhs, ) . one . (lhs, , 0) <$> double

-- | Parse a single consisting of a lexical item of the form @A b w@ into some
-- structure indexed by the left hand side, i.e. the A.
pLexicalLeft :: Parser (ByteString, Set Lexical)
pLexicalLeft = skipSpace *> go <* end
  where
    -- | Parse @A b w@ into the left-indexed form @(A, {A, b, w})@.
    go :: Parser (ByteString, Set Lexical)
    go = do
        lhs <- takeSkipSpace1
        (lhs, ) . one <$> (Lexical lhs <$> takeSkipSpace1 <*> double)

{- | Parse a single line consisting of a non-lexical binary or unary rule
   (that is, @A -> B C w@ or @A -> B w@ respectively).

   We parse these into a type consisting of maps, such that we can preserve the
   arity of the rule and index it by its right-hand side non-terminal (in case
   of a binary rule we generate two key-value pairs for two distinct maps).
-}
pNonLexical :: Parser RhsAccess
pNonLexical = skipSpace *> pNonLexWith makeNL1 makeNL2 <* end

{- | Parse a single line consisting of a non-lexical binary or unary rule (that
   is, @A -> B C w@ or @A -> B w@ respectively) into the respective left-access
   constructions.
-}
pNonLexicalLeft :: Parser LhsAccess
pNonLexicalLeft = skipSpace *> pNonLexWith makeNL1Left makeNL2Left <* end

-- | Parse a non-lexical rule and order the results according to some given
-- parameters.
pNonLexWith :: forall a. (NonLexical1 -> a) -> (NonLexical2 -> a) -> Parser a
pNonLexWith make1 make2 = do
    lhs <- takeSkipSpace1 <* "->" <* skipSpace
    pRight lhs =<< takeSkipSpace1
  where
      -- | Given A and B, parse either w or @C w@ and generate the respective
      -- maps.
      pRight :: ByteString -> ByteString -> Parser a
      pRight lhs rhs1
           =  make1 <$> (NonLexical1 lhs rhs1 <$> double <*> pure 0)
          <|> make2 <$> (NonLexical2 lhs rhs1
                             <$> takeSkipSpace1 <*> double <*> pure 0)

-- | Parse a /.outside/ file, consisting of lines of the form @A w@.
pOutside :: Parser (ByteString, Double)
pOutside = skipSpace *> go <* end
  where
    go :: Parser (ByteString, Double)
    go = (,) <$> takeSkipSpace1 <*> double

-- | Construct a right hand side access map from a single lexical rule.
makeNL1 :: NonLexical1 -> RhsAccess
makeNL1 r@NonLexical1{ rhs } = RhsAccess (fone (rhs, r)) mempty mempty

-- | Construct a left hand side access map from a single lexical rule.
makeNL1Left :: NonLexical1 -> LhsAccess
makeNL1Left r@NonLexical1{ lhs } = LhsAccess (fone (lhs, r)) mempty (one (lhs, 0))

-- | Construct a right hand side access map from a single non-lexical rule.
makeNL2 :: NonLexical2 -> RhsAccess
makeNL2 r@NonLexical2{ rhs1, rhs2 } =
    RhsAccess mempty (fone (rhs1, r)) (fone (rhs2, r))

-- | Construct a left hand side access map from a single non-lexical rule.
makeNL2Left :: NonLexical2 -> LhsAccess
makeNL2Left r@NonLexical2{ lhs } = LhsAccess mempty (fone (lhs, r)) (one (lhs, 0))

-- | Helper function for parsing words separated by spaces.
takeSkipSpace1 :: Parser ByteString
takeSkipSpace1 = takeWhile1 (/= ' ') <* skipSpace

-- | Apply 'one' to some items inside a functor, as well as the result of that
-- computation (i.e. the functorial result).
fone :: (Functor f, One a, One b, OneItem b ~ f a) => f (OneItem a) -> b
fone item = one (one <$> item)

{- | Often we would like to apply a parser until we have either reached the end
   of the line or the end of the input (as is the case when parsing a file that
   lacks a final newline).
-}
end :: Parser ()
end = endOfInput <|> endOfLine
