{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE StrictData           #-}

{- |
   Module      : Parse.Types
   Description : Types for parsing context-free grammars.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Parse.Types
    ( -- * The rules themselves
      Lexical(..)      -- instances: Show, Eq, Ord, Generic, Hashable
    , NonLexical1(..)  -- instances: Show, Eq, Generic, Hashable
    , NonLexical2(..)  -- instances: Show, Eq, Generic, Hashable

      -- * Rule access from the right or left hand side
    , NL1(..)          -- instances: Show, Semigroup, Monoid, One
    , NL2(..)          -- instances: Show, Semigroup, Monoid, One
    , NL2Left(..)      -- instances: Show, Semigroup, Monoid, One
    , NL2Right(..)     -- instances: Show, Semigroup, Monoid, One
    , LeftLex(..)      -- instances: Show, Semigroup, Monoid, One
    , RhsAccess(..)    -- instances: Show, Semigroup, Monoid
    , LhsAccess(..)    -- instances: Show, Semigroup, Monoid

      -- * Found items
    , Found(..)        -- instances: Eq, Generic, Hashable

      -- * Access to the found items from either side
    , WeightMap        -- type alias: HashMap Found Weight
    , LeftAccess       -- type alias: HashMap (Int, ByteString) WeightMap
    , RightAccess      -- type alias: HashMap (ByteString, Int) WeightMap

      -- * Backtraces
    , Backtrace(..)    -- instances: Eq, Generic, Hashable
    , Backtraces       -- type alias: HashMap Found Backtrace

      -- * Parsing files
    , LexItem          -- type alias: (ByteString, NonEmpty (ByteString, Weight, Weight))
    , LexRules         -- type alias: HashMap ByteString (NonEmpty (ByteString, Weight, Weight))

      -- * Outside weights
    , OutWeights       -- type alias: HashMap ByteString Weight
    ) where

import Core.Util (Weight)

import qualified Data.HashMap.Strict as Map


{- The following three definitions are for differentiating between different
   types of rules at the type level.  There are some name clashes with data
   constructors from "Grammar.Types", though for readability reasons we consider
   this to be a trade-off worth making.
-}

-- | Rule of the form @A b w@.
data Lexical = Lexical
    { lhs :: ByteString
    , rhs :: ByteString
    , w   :: Weight
    }
    deriving (Show, Eq, Ord, Generic, Hashable)

-- | Rule of the form @A ⟶ B w@.
data NonLexical1 = NonLexical1
    { lhs  :: ByteString
    , rhs  :: ByteString
    , w    :: Weight
    , outW :: Weight
    }
    deriving (Show, Eq, Generic, Hashable)

-- | Rule of the form @A ⟶ B C w@.
data NonLexical2 = NonLexical2
    { lhs  :: ByteString
    , rhs1 :: ByteString
    , rhs2 :: ByteString
    , w    :: Weight
    , outW :: Weight
    }
    deriving (Show, Eq, Generic, Hashable)

-- | The following type aliases will be used when referring to lexical rules.
type LexItem  = (ByteString, NonEmpty (ByteString, Weight, Weight))
type LexRules = HashMap ByteString (NonEmpty (ByteString, Weight, Weight))

-- | Internal helpers for types pertaining to /.rules/ files.
type NonLexRules1 = HashMap ByteString (HashSet NonLexical1)
type NonLexRules2 = HashMap ByteString (HashSet NonLexical2)

{- | When looking up new rules, it is convenient to be able to quickly refer to
   the left or right hand side of a rule.  The following newtypes are handy for
   exactly specifying __which__ left/right hand side one is referring to and
   doing so at the type level.
-}
newtype NL1 = NL1 NonLexRules1            -- Either the A or the B in A ⟶ B
    deriving newtype (Show, One)

newtype NL2 = NL2 NonLexRules2            -- The A in A ⟶ B C
    deriving newtype (Show, One)

newtype NL2Left = NL2Left NonLexRules2    -- The B in A ⟶ B C
    deriving newtype (Show, One)

newtype NL2Right = NL2Right NonLexRules2  -- The C in A ⟶ B C
    deriving newtype (Show, One)

newtype LeftLex = LeftLex (HashMap ByteString (Set Lexical))  -- The A in A b
    deriving newtype (Show, One)

{- | 'Semigroup' and 'Monoid' instances that combine the values for duplicate
   keys in a useful way.

   'Semigroup' instances must fulfill the associative law:

   prop> (a <> b) <> c = a <> (b <> c)

   'Monoid' instances must fulfill neutrality with regards to 'mempty':

   prop> a <> mempty = a
   prop> mempty <> a = a

   The fact that this holds for all of the following instances follows directly
   from the fact that 'HashSet' and 'Set' have law abiding 'Semigroup' and
   'Monoid' instances.
-}
instance Semigroup NL1 where
    (<>) :: NL1 -> NL1 -> NL1
    m <> m' = under2 @NonLexRules1 (Map.unionWith (<>)) m m'

instance Monoid NL1 where
    mempty :: NL1
    mempty = NL1 mempty

instance Semigroup NL2 where
    (<>) :: NL2 -> NL2 -> NL2
    m <> m' = under2 @NonLexRules2 (Map.unionWith (<>)) m m'

instance Monoid NL2 where
    mempty :: NL2
    mempty = NL2 mempty

instance Semigroup NL2Left where
    (<>) :: NL2Left -> NL2Left -> NL2Left
    m <> m' = under2 @NonLexRules2 (Map.unionWith (<>)) m m'

instance Monoid NL2Left where
    mempty :: NL2Left
    mempty = NL2Left mempty

instance Semigroup NL2Right where
    (<>) :: NL2Right -> NL2Right -> NL2Right
    m <> m' = under2 @NonLexRules2 (Map.unionWith (<>)) m m'

instance Monoid NL2Right where
    mempty :: NL2Right
    mempty = NL2Right mempty

instance Semigroup LeftLex where
    (<>) :: LeftLex -> LeftLex -> LeftLex
    m <> m' = under2 @(HashMap ByteString (Set Lexical)) (Map.unionWith (<>)) m m'

instance Monoid LeftLex where
    mempty :: LeftLex
    mempty = LeftLex mempty

-- | Access to all rhs non-terminals from a single product type.
data RhsAccess = RhsAccess
    { nl1      :: NL1
    , nl2left  :: NL2Left
    , nl2right :: NL2Right
    } deriving (Show)

-- | Access to all lhs non-terminals from a single product type.
data LhsAccess = LhsAccess
    { nl1 :: NL1
    , nl2 :: NL2
    , nts :: HashMap ByteString Weight
    } deriving (Show)

{- | The following instances for 'Monoid' and 'Semigroup' clearly satisfy

   prop> (a <> b) <> c = a <> (b <> c)
   prop> a <> mempty = a
   prop> mempty <> a = a

   as they are just wrappers around the respective instances for the newtypes
   (which, in turn, wrap around the 'Map' instances).  They are written out here
   because of the rather arbitrary decision of GHC to not make them derivable
   (for non-newtypes), due to the possibility of them not being unique (which is
   also the case for 'Functor' and yet there is 'DeriveFunctor').
-}
instance Semigroup RhsAccess where
    (<>) :: RhsAccess -> RhsAccess -> RhsAccess
    RhsAccess a b c <> RhsAccess a' b' c' =
        RhsAccess (a <> a') (b <> b') (c <> c')

instance Monoid RhsAccess where
    mempty :: RhsAccess
    mempty = RhsAccess mempty mempty mempty

instance Semigroup LhsAccess where
    (<>) :: LhsAccess -> LhsAccess -> LhsAccess
    LhsAccess a b c <> LhsAccess a' b' c' =
        LhsAccess (a <> a') (b <> b') (c <> c')

instance Monoid LhsAccess where
    mempty :: LhsAccess
    mempty = LhsAccess mempty mempty mempty

{- | Backtrace for keeping track of __how__ we derived a dequeued element.  Note
   that, because the edges of a hypergraph have to \"fit together\" when they
   share a node, we only need to store three index values for a binary rule.
-}
data Backtrace
    = Term  Lexical
    | Chain NonLexical1 Int Int
    | Bin   NonLexical2 Int Int Int
    deriving (Eq, Generic, Hashable)

-- | Backtraces for an entire sentence.
type Backtraces = HashMap Found Backtrace

-- | Found item (i.e. item with the highest weight in the queue at this time).
data Found = Item
    { nt    :: ~ByteString  -- ^ A, for an arbitrary rule @A ⟶ α@
    , left  :: Int          -- ^ Left span
    , right :: Int          -- ^ Right span
    }
    deriving (Eq, Generic, Hashable)

{- | Associate a found item with its weight, but do this in a way such that we
   still have the possibility to consider the item without that weight.  This is
   needed, for example, when considering whether a found item is \"new\" in the
   sense that it has never been seen before.
-}
type WeightMap = HashMap Found Weight

-- | Accessing the found items from either side.
type LeftAccess  = HashMap (Int, ByteString) WeightMap
type RightAccess = HashMap (ByteString, Int) WeightMap

-- | Convenience type for referring to outside weights.
type OutWeights = HashMap ByteString Weight
