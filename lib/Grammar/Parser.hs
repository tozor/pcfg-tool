{- |
   Module      : Grammar.Parser
   Description : Parse an S-expression into a tree.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Parser
    ( pTree  -- :: Parser PennTree
    ) where

import Core.Tree (PennForest, PennTree, Tree(Node))

import Data.Attoparsec.ByteString (Parser)
import Data.Attoparsec.ByteString.Char8
    ( char
    , endOfInput
    , endOfLine
    , skipSpace
    , takeWhile1
    )


-- | Parse an S-expression that is written on a single line into a tree.  By
-- this property, we may ensure balanced parentheses of the given expression.
pTree :: Parser PennTree
pTree = pTree' <* (endOfLine <|> endOfInput)

{- | Parse a single 'PennTree' from an S-expression.
   In the following, let "word" always mean "terminal or non-terminal symbol".
   There are several assumptions made here, so as to make parsing an
   S-expression into a tree possible:
     - Words may not contain a space, nor either parenthesis character.
     - Every S-expression must contain at least one word at its topmost level.
       This means expressions of the form /()/ or /((VB fail))/ are prohibited.
-}
pTree' :: Parser PennTree
pTree' = skipSpace *>
     (  char '(' *> skipSpace *> (Node <$> pWord <*> pForest)
    <|> Node <$> pWord <*> pure []
     )

-- | Helper function for parsing a forest.
pForest :: Parser PennForest
pForest =  skipSpace *> char ')' $> []
       <|> (:) <$> pTree' <*> pForest

-- | Parse a single terminal or non-terminal symbol.
pWord :: Parser ByteString
pWord = takeWhile1 \w -> w /= '('
                      && w /= ')'
                      && w /= ' '
