{- |
   Module      : Grammar.Induction
   Description : Module for grammar induction.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Induction
    ( runInduction  -- :: Maybe FilePath -> IO ()
    ) where

import Core.Util (ByteStream, streamParse)
import Grammar.Parser (pTree)
import Grammar.Types (Grammar, Rule(lrhs))
import Grammar.Util (insertRulesInto, isLexical, normalize, prettyGrammar)

import qualified Data.ByteString.Char8           as BS
import qualified Data.ByteString.Streaming.Char8 as SB
import qualified Data.HashMap.Strict             as Map
import qualified Streaming.Prelude               as S

import Control.Concurrent.Async (mapConcurrently_)


-- | IO interface for the /induce/ command.
runInduction :: Maybe FilePath -> IO ()
runInduction mbFile = do
    grammar <- induce SB.stdin

    let lexical = Map.filterWithKey (const . isLexical) grammar
        bsLex   = prettyGrammar lexical

    let bsNonLex = prettyGrammar $
            Map.filterWithKey (const . not . isLexical) grammar

    -- We can use an unstable algorithm to remove duplicates here, as we do not
    -- care in which order the words are written to the file.
    let terminals = BS.unlines . unstableNub . map lrhs . Map.keys $ lexical

    -- Print the results according to the users preferences.
    case mbFile of
        Nothing -> traverse_ putBSLn [bsNonLex, bsLex, terminals]
        Just f  -> mapConcurrently_ (uncurry writeFileBS)
            [ (f <> ".rules"  , bsNonLex)
            , (f <> ".lexicon", bsLex)
            , (f <> ".words"  , terminals)
            ]

-- | Parse S-expressions, generate a grammar for every tree and merge them all
-- together.  Also see Note [Stream].
induce :: Monad m => ByteStream m r -> m Grammar
induce stream =
    streamParse stream pTree (S.fold_ insertRulesInto mempty normalize)

{- Note [Stream]
   ~~~~~~~~~~~~~~~~~~~~~~
   The furthest one can get away with streaming is up until all rules are read
   off the trees.  After that one has to consider the grammar of the whole
   corpus instead of treating every sentence separate, which is not possible
   within the coalgebraic structure of a stream (a singleton stream might as
   well not be one).

   Hence, the employed fold completely consumes the stream---it reads the rules
   off of every tree and builds one single grammar from them.  The extraction
   function (as the name suggests) normalizes our grammar.  This means no
   constant memory usage from there on out and we have to allocate the whole
   hash map.
-}
