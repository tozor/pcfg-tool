{- |
   Module      : Grammar.Debinarise
   Description : Interface for the /debinarise/ command.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Debinarise
    ( runDebinarise  -- :: IO ()
    ) where

import Core.Util (streamInOut)
import Grammar.Util (debinarise)


-- | 'IO' interface for the /debinarise/ command.
runDebinarise :: IO ()
runDebinarise = streamInOut debinarise
