{-# LANGUAGE StrictData #-}

{- |
   Module      : Grammar.Types
   Description : Types for working with the Penn Treebank.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Types
    ( -- * Rule and grammar types
      Rule(..)   -- instances: Eq, Generic, Hashable
    , Grammar    -- type alias: HashMap Rule Weight

      -- * Misc
    , WordCount  -- type alias: HashMap ByteString Int
    ) where

import Core.Util (Weight)


-- | A rule may be lexical or non-lexical.
data Rule
    = Lexical
          { lhs  :: ByteString
          , lrhs :: ByteString
          }
    | NonLexical
          { lhs   :: ByteString
          , nlrhs :: [ByteString]
          }
    deriving (Eq, Generic, Hashable)

{- | A 'HashMap' to represent a grammar.  Hashing elements is often faster than
   trying to create a balanced tree from them via an 'Ord' instance.  It is also
   an immutable (really: persistent) data structure, allowing for more idiomatic
   code.

   This type is value strict (i.e. we use a strict 'HashMap' with a strict key
   type).  We are actively working with the grammar (notably iterating over it
   multiple times) and thus we would prefer it if this type did not produce any
   thunks.

   Alas, due to automated testing we are forced to use a floating point type
   here, instead of something more "correct", like @Ratio Int@.
-}
type Grammar = HashMap Rule Weight

-- | Convenient type alias for talking about word counts.
type WordCount = HashMap ByteString Int
