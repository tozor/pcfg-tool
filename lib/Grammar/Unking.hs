{- |
   Module      : Grammar.Unking
   Description : Perform trivial unking on a given corpus.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Unking
    ( runUnking  -- :: Int -> IO ()
    ) where

import Core.Util (streamFileOut, streamParse)
import Grammar.Parser (pTree)
import Grammar.Util (unkLeaves, wordCount)

import qualified Data.ByteString.Streaming.Char8 as SB
import qualified Streaming.Prelude               as S

import Control.Exception (catch)
import Control.Monad.Trans.Resource (runResourceT)
import System.Directory (removeFile)


-- | The 'IO' interface for the /unking/ command.  See Note [Stream].
runUnking :: Int -> IO ()
runUnking thresh = do
    -- Temp file to store trees in.
    let file = ".unk-trees-temp"
    runResourceT $ SB.stdin & SB.writeFile file

    -- Get word count map.
    wc <- runResourceT $
        streamParse (SB.readFile file) pTree (S.fold_ wordCount mempty id)

    -- Unk trees and print to stdout.
    streamFileOut file (unkLeaves wc thresh)

    -- Remove file if possible, otherwise just print the error.
    removeFile file `catch` \(e :: SomeException) -> print e

{- Note [Stream]
   ~~~~~~~~~~~~~~~~~~~~~~
   If streams are not consumed on the first pass, the whole object has to be
   allocated to memory (either as a list or explicitly as a stream).  Because
   streams are really only fast if they can fuse, this makes the whole process
   much slower and more memory intensive.

   Due to this we prefer to send the given input to a file and then read from
   that file as often as we need to, in order to guarantee linear consumption of
   streams.
-}
