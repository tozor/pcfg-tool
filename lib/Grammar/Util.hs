{- |
   Module      : Grammar.Util
   Description : Utility functions for working with a grammar.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Util
    ( -- * Induction
      normalize        -- :: Grammar -> Grammar
    , insertRulesInto  -- :: Grammar -> PennTree -> Grammar
    , prettyGrammar    -- :: Grammar -> ByteString
    , isLexical        -- :: Rule -> Bool

      -- * Debinarisation
    , debinarise       -- :: PennTree -> PennTree

      -- * Unking
    , wordCount        -- :: WordCount -> PennTree -> WordCount
    , countLeaves      -- :: PennTree -> WordCount
    , unkLeaves        -- :: WordCount -> Int -> PennTree -> PennTree

      -- * Binarisation
    , binarise         -- :: Horizontal -> Vertical -> PennTree -> PennTree
    ) where

import Core.Tree
    ( PennForest
    , PennTree
    , Tree(Leaf, Node, Preterminal, root)
    , leaves
    , mapLeaves
    )
import Core.Util
    ( Horizontal(Horizontal)
    , Vertical(Vertical)
    , mconcatMap
    , unsafeUnsnoc
    )
import Grammar.Types (Grammar, Rule, Rule(Lexical, NonLexical, lhs), WordCount)

import qualified Data.ByteString.Char8 as BS
import qualified Data.HashMap.Strict   as Map

import Data.Double.Conversion.ByteString (toShortest)
import Data.HashMap.Strict ((!), (!?))


{- We decided to monomorphise most functions in this module, as type signatures
   such as
   @
     normalize
         :: forall n t w. (Eq n, Hashable n, Fractional w)
         => HashMap (Rule n t) w
         -> HashMap (Rule n t) w
         [...]
       where occurs :: HashMap n w
         [...]
   @
   are (in our opinion) neither particularly enlightening, nor do we actually
   need this much polymorphism in this application for now.
-}

-- | Normalize the weights of a given 'Grammar' by calculating the relative
-- frequencies of the non-terminal symbols.
normalize :: Grammar -> Grammar
normalize grammar = Map.mapWithKey (\r w -> w / occurs ! lhs r) grammar
                    {- '!' is safe here, as 'occurs' contains the rule 'r' if
                       (and only if) it is in 'grammar' (and 'mapWithKey' short
                       circuits if 'grammar' is empty). -}
  where
    -- | Generate a new map that counts how often each non-terminal symbol
    -- appears on the left side of a rule.  This fuses rather nicely.
    occurs :: HashMap ByteString Double
    occurs = Map.fromListWith (+) . map (first lhs) . toList $ grammar

-- | Merge the grammar you get from reading the rules off a tree into the given
-- grammar.
insertRulesInto :: Grammar -> PennTree -> Grammar
insertRulesInto grammar = Map.unionWith (+) grammar . readoff

-- | Read the rules off a tree and directly construct a grammar from it.
readoff :: PennTree -> Grammar
readoff = \case
    Leaf _           -> mempty  -- This should never happen.
    Preterminal r sr -> one (Lexical r sr, 1)
    Node r sf        ->
        foldl' (Map.unionWith (+))
               (one (NonLexical r (map root sf), 1))
               (map readoff sf)

-- | Pretty print a 'Grammar' as per the given specifications.
prettyGrammar :: Grammar -> ByteString
prettyGrammar = mconcatMap (prettyRule . fmap toShortest) . toList
  where
    -- | Pretty print a pair consisting of a rule and its associated weight,
    -- already converted to 'ByteString'.
    prettyRule :: (Rule, ByteString) -> ByteString
    prettyRule = mconcat . \case
        (Lexical    n t , w) -> [n, " ", t, " ", w, "\n"]
        (NonLexical n ns, w) -> [n, " -> ", BS.unwords ns, " ", w, "\n"]

-- | Check whether a rule is lexical.
isLexical :: Rule -> Bool
isLexical = \case
    Lexical{} -> True
    _         -> False

-- | Update a word count map with the leaves of a given tree.
wordCount :: WordCount -> PennTree -> WordCount
wordCount wc = Map.unionWith (+) wc . countLeaves

-- | Count the all leaves in a single tree.
countLeaves :: PennTree -> WordCount
countLeaves tree = Map.fromListWith (+) [(l, 1) | l <- leaves tree]

-- | Replace uncommon words with __UNK__.
unkLeaves :: WordCount -> Int -> PennTree -> PennTree
unkLeaves wc thresh = mapLeaves \leaf -> case wc !? leaf of
    Nothing    -> "UNK"  -- This should never happen for a proper word count.
    Just count -> if count <= thresh then "UNK" else leaf

-- | Debinarise a tree by removing all markovized nodes and stripping ancestor
-- annotation from them if necessary.
debinarise :: PennTree -> PennTree
debinarise = \case
    Leaf r           -> Leaf r
    Preterminal r r' -> Preterminal (originalLabel r) r'
    Node r sf        -> if   '|' `BS.elem` r'  -- markov node
                        then debinarise $ Node r (sf' ++ lastTwo)
                        else Node (originalLabel r) (map debinarise sf)
      where
        (Node r' lastSf, sf') = unsafeUnsnoc sf
        -- This is safe because we already know that 'sf' is non-empty at this
        -- point and hence it contains at least one element.

        lastTwo :: PennForest
        lastTwo = take 2 lastSf

-- | Binarize a given tree according to a horizontal and a vertical threshold.
binarise :: Horizontal -> Vertical -> PennTree -> PennTree
binarise h v = go []
  where
    go :: [ByteString] -> PennTree -> PennTree
    go parents = \case
        t@Preterminal{} -> t
        Node r [t] ->
            Node (addParents v r parents) [go (originalLabel r : parents) t]
        Node r [Node r1 sf1, Node r2 sf2] ->
            Node (addParents v r parents)
                 (go (originalLabel r : parents) <$> [Node r1 sf1, Node r2 sf2])
        Node r (tree:sf) ->
            Node (addParents v r parents)
                 [ go (originalLabel r : parents) tree
                 , go parents $ Node (markovNode h r sf) sf
                 ]
        t -> t

-- | Add the requested number of parents (by the vertical markovization factor)
-- to a certain node.
addParents :: Vertical -> ByteString -> [ByteString] -> ByteString
addParents (Vertical v) node parents
    | null parents || v <= 1 = node
    | otherwise =
        mconcat [node, "^<", BS.intercalate "," $ take (v - 1) parents, ">"]

-- | Horizontal markovization when adding a new node.
markovNode :: Horizontal -> ByteString -> [PennTree] -> ByteString
markovNode (Horizontal h) r sf = mconcat
    [ originalLabel r
    , "|<"
    , BS.intercalate "," $ take (h + 1) (map root sf)
    , ">"
    ]

-- | Recover the original label of the node by stripping vertical and horizontal
-- annotations.
originalLabel :: ByteString -> ByteString
originalLabel = BS.takeWhile \w -> w /= '|' && w /= '^'
