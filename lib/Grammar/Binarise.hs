{- |
   Module      : Grammar.Binarise
   Description : Interface for the /binarise/ command.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Grammar.Binarise
    ( runBinarise  -- :: Horizontal -> Vertical -> IO ()
    ) where

import Core.Util (Horizontal, Vertical, streamInOut)
import Grammar.Util (binarise)


-- | 'IO' interface for the /binarise/ command.
runBinarise :: Horizontal -> Vertical -> IO ()
runBinarise = (streamInOut .) . binarise
