{-# LANGUAGE StrictData #-}

{- |
   Module      : Core.CLI
   Description : The tools command line interface.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Core.CLI
    ( Commands(..)
    , ParseOptions(..)
    , Paradigm(..)      -- instances: Eq
    , commands          -- :: ParserInfo (Maybe Commands)
    ) where

import Core.Util (Horizontal(Horizontal), Vertical(Vertical))

import qualified Data.Attoparsec.Text as A

import Options.Applicative
    ( Parser
    , ParserInfo
    , ReadM
    , argument
    , auto
    , command
    , eitherReader
    , fullDesc
    , header
    , help
    , helper
    , hsubparser
    , info
    , long
    , metavar
    , option
    , short
    , str
    , strOption
    , switch
    , value
    )


-- | Commands the user may give to the tool.
data Commands
    = Induce (Maybe FilePath)
    | Parse
          { opts    :: ParseOptions
          , rules   :: FilePath
          , lexicon :: FilePath
          }
    | Binarise
          { horizontal :: Horizontal
          , vertical   :: Vertical
          }
    | Debinarise
    | Unk { threshold :: Int
          }
    | Smooth
          { threshold :: Int
          }
    | Outside
          { initNT  :: ByteString
          , rules   :: FilePath
          , lexicon :: FilePath
          , grammar :: Maybe FilePath
          }

-- | Build the command line interface out of all available options, add help
-- texts and other niceties to it.
commands :: ParserInfo (Maybe Commands)
commands = info
    (helper <*> pCommands)
    (  header "Tools zum PCFG-basierten Parsing natürlichsprachiger Sätze"
    <> fullDesc
    )

{- | As per the specification, the constructors of the 'Commands' type will be
   available via subcommands.  Here we also make it optional to specify a
   command at all.
-}
pCommands :: Parser (Maybe Commands)
pCommands = optional . hsubparser $ mconcat
    [ command "induce"     . info pInduce     $ header induceHeader
    , command "parse"      . info pParse      $ header parseHeader
    , command "binarise"   . info pBinarise   $ header binariseHeader
    , command "debinarise" . info pDebinarise $ header debinariseHeader
    , command "unk"        . info pUnk        $ header unkHeader
    , command "smooth"     . info pSmooth     $ header smoothHeader
    , command "outside"    . info pOutside    $ header outsideHeader
    ]
  where
    induceHeader :: String
    induceHeader =
        "Liest eine Sequenz Konstituentenbäume von der Standardeingabe und \
        \gibt eine aus diesen Bäumen induzierte PCFG auf der Standardausgabe \
        \aus.  Wird das optionale Argument GRAMMAR angegeben, dann wird die \
        \PCFG stattdessen in den Dateien GRAMMAR.rules, GRAMMAR.lexicon und \
        \GRAMMAR.words gespeichert."

    parseHeader :: String
    parseHeader =
        "Liest eine Sequenz natürlichsprachiger Sätze von der Standardeingabe \
        \und gibt die zugehörigen besten Parsebäume im PTB-Format bzw. \
        \(NOPARSE <Satz>) auf der Standardausgabe aus.  RULES und LEXICON sind \
        \die Dateinamen der PCFG."

    binariseHeader :: String
    binariseHeader =
        "Liest eine Sequenz Konstituentenbäume von der Standardeingabe und \
        \gibt die entsprechenden binarisierten Konstituentenbäume auf der \
        \Standardausgabe aus."

    debinariseHeader :: String
    debinariseHeader =
        "Liest eine Sequenz (binarisierter) Konstituentenbäume von der \
        \Standardeingabe und gibt die ursprünglichen (nichtbinarisierten) \
        \Konstituentenbäume auf der Standardausgabe aus."

    unkHeader :: String
    unkHeader =
        "Liest eine Sequenz Konstituentenbäume von der Standardeingabe und \
        \gibt die durch triviales Unking erhaltenen Bäume auf der \
        \Standardausgabe aus."

    smoothHeader :: String
    smoothHeader =
        "Liest eine Sequenz Konstituentenbäume von der Standardeingabe und \
        \gibt die durch Smoothing erhaltenen Bäume auf der Standardausgabe aus."

    outsideHeader :: String
    outsideHeader =
        "Berechnet Viterbi Outside weights für jedes Nichtterminalder \
        \Grammatik und gibt diese auf der Standardausgabe aus.  Wird das \
        \optionale Argument GRAMMAR angegeben, dann werden die Outside weights \
        \in die Datei GRAMMAR.outside gespeichert."

-- | Parser for the 'Induce' data constructor.
pInduce :: Parser Commands
pInduce = Induce <$> optional (argument str (metavar "GRAMMAR"))

-- | Additional options and flags to give to the `parse' command.
data ParseOptions = ParseOptions
    { paradigm  :: Paradigm
    , initNT    :: ByteString
    , unking    :: Bool
    , smoothing :: Bool
    , thBeam    :: Maybe Double
    , rkBeam    :: Maybe Int
    , kbest     :: Int
    , astar     :: Maybe FilePath
    }

-- | With which method to parse a given context-free grammar.
data Paradigm = CYK | Deductive deriving (Eq)

-- | Parser for the 'Parse' data constructor.
pParse :: Parser Commands
pParse =  Parse
      <$> pOpts
      <*> argument str (metavar "RULES")
      <*> argument str (metavar "LEXICON")
  where
    pOpts :: Parser ParseOptions
    pOpts =  ParseOptions
         <$> pParadigm
         <*> pInitNT
         <*> pUnking
         <*> pSmoothing
         <*> pThreshBeam
         <*> pRkBeam
         <*> pKbest
         <*> pAstar

    -- | This allows for the paradigm to be specified by a single letter while
    -- still conforming to the specifications
    pParadigm :: Parser Paradigm
    pParadigm = option (attoReadM ("c" $> CYK <|> "d" $> Deductive))
         ( long "paradigma"
        <> short 'p'
        <> metavar "PARADIGMA"
        <> help "Verwendetes Parserparadigma (cyk oder deductive).  \
                \Default: deductive."
        <> value Deductive
         )

    pInitNT :: Parser ByteString
    pInitNT = strOption
         ( long "initial-nonterminal"
        <> short 'i'
        <> metavar "N"
        <> help "Definiere N als Startnichtterminal.  Default: ROOT"
        <> value "ROOT"
         )

    pUnking :: Parser Bool
    pUnking = switch
         ( long "unking"
        <> short 'u'
        <> help "Ersetze unbekannte Wörter durch UNK"
         )

    pSmoothing :: Parser Bool
    pSmoothing = switch
         ( long "smoothing"
        <> short 's'
        <> help "Ersetze unbekannte Wörter gemäß der Smoothing-Implementierung."
         )

    pThreshBeam :: Parser (Maybe Double)
    pThreshBeam = optional $ option auto
         ( long "threshold-beam"
        <> short 't'
        <> metavar "THRESHOLD"
        <> help "Führe Beam-Search durch mit THRESHOLD."
         )

    pRkBeam :: Parser (Maybe Int)
    pRkBeam = optional $ option auto
         ( long "rank-beam"
        <> short 'r'
        <> metavar "RANK"
        <> help "Führe Beam-Search durch mit Beam konstanter Größe."
         )

    pKbest :: Parser Int
    pKbest = option auto
         ( long "kbest"
        <> short 'k'
        <> metavar "K"
        <> help "Gib K beste Parsebäume statt des besten aus."
        <> value 1
         )

    pAstar :: Parser (Maybe FilePath)
    pAstar = optional $ strOption
         ( long "astar"
        <> short 'a'
        <> metavar "PATH"
        <> help "Führe A*-Suche durch. Berechne zuvor Outside weights für \
                \jedes Nichtterminal. Wird hingegen das optionale Argument \
                \PATH angegeben , so werden die Outside weights von diesem \
                \Pfad geladen."
         )

-- | Parser for the 'Binarise' data constructor.
pBinarise :: Parser Commands
pBinarise = Binarise <$> pHorizontal <*> pVertical
  where
    pHorizontal :: Parser Horizontal
    pHorizontal = Horizontal <$> option auto
         ( long "horizontal"
        <> short 'h'
        <> metavar "H"
        <> help "Horizontable Markovisierung mit H."
        <> value 999
         )

    pVertical :: Parser Vertical
    pVertical = Vertical <$> option auto
         ( long "vertical"
        <> short 'v'
        <> metavar "V"
        <> help "Vertikale Markovisierung mit V."
        <> value 1
         )

-- | Parser for the 'Debinarise' data constructor.
pDebinarise :: Parser Commands
pDebinarise = pure Debinarise

-- | Parser for the 'Unk' data constructor.
pUnk :: Parser Commands
pUnk = Unk <$> pThreshold
  where
    pThreshold :: Parser Int
    pThreshold = option auto
         ( long "threshold"
        <> short 't'
        <> metavar "T"
        <> help "Schwellwert der absoluten Häufigkeit für das Unking."
        <> value 1
         )

-- | Parser for the 'Smooth' data constructor.
pSmooth :: Parser Commands
pSmooth = Smooth <$> pThreshold
  where
    pThreshold :: Parser Int
    pThreshold = option auto
         ( long "threshold"
        <> short 't'
        <> metavar "T"
        <> help "Schwellwert der absoluten Häufigkeit für das Unking."
        <> value 1
         )

-- | Parser for the 'Outside' data constructor.
pOutside :: Parser Commands
pOutside =  Outside
        <$> pInitNT
        <*> argument str (metavar "RULES")
        <*> argument str (metavar "LEXICON")
        <*> optional (argument str (metavar "GRAMMAR"))
  where
    pInitNT :: Parser ByteString
    pInitNT = strOption
         ( long "initial-nonterminal"
        <> short 'i'
        <> metavar "N"
        <> help "Definiere N als Startnichtterminal."
        <> value "ROOT"
         )

-- | Attoparsec <--> optparse-applicative interface.
attoReadM :: A.Parser a -> ReadM a
attoReadM p = eitherReader (A.parseOnly p . fromString)
