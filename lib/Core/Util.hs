{- |
   Module      : Core.Util
   Description : General utility types and functions
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Core.Util
    ( -- * Types
      ByteStream      -- type alias: Data.ByteString.Streaming.Char8.ByteString
    , Weight          -- type alias: Double
    , Horizontal(..)  -- newtype wrapper: Int
    , Vertical(..)    -- newtype wrapper: Int

      -- * Misc
    , mconcatMap      -- :: Monoid m => (a -> m) -> [a] -> m
    , unsafeUnsnoc    -- :: [a] -> (a, [a])

      -- * Parallelism
    , parMapN         -- :: Int -> Strategy b -> (a -> b) -> [a] -> [b]

      -- * Working with streams
    , runStream       -- :: Identity a -> a
    , streamInOut     -- :: (PennTree -> PennTree) -> IO ()
    , streamFileOut   -- :: FilePath -> (PennTree -> PennTree) -> IO ()
    , streamParse     -- :: Monad m => Parser a -> ByteStream m r -> (Stream (Of a) m () -> b) -> b
    ) where

import Core.Tree (PennTree, prettyTree)
import Grammar.Parser (pTree)

import qualified Data.ByteString.Streaming.Char8 as SB
import qualified Streaming.Prelude               as S

import Control.Monad.Trans.Resource (runResourceT)
import Control.Parallel.Strategies (Strategy, parListChunk, using)
import Data.Attoparsec.ByteString (Parser)
import Data.Attoparsec.ByteString.Streaming (parsed)
import Streaming (Of, Stream)


-- | Less confusion as to which 'ByteString' one is referring to.
type ByteStream = SB.ByteString

-- | This makes type signatures a bit more expressive.
type Weight = Double

-- | Newtypes for horizontal and vertical markovization in order to make it
-- impossible to mix these two up.
newtype Horizontal = Horizontal Int
newtype Vertical   = Vertical   Int

-- | Same as 'concatMap', but for monoids instead of lists.
mconcatMap :: Monoid m => (a -> m) -> [a] -> m
mconcatMap = (mconcat .) . map

{- | Unsnoc a list.  Returns an error if the input list was empty.  While this
   is of course @O(n)@, it will solely be used for trees and the lists for any
   given tree are rather small, so we do not need to pay __too__ much attention
   to this.

   Adapted from 'Data.List.init', this results in __beautiful__ core.
-}
unsafeUnsnoc :: [a] -> (a, [a])
unsafeUnsnoc = \case
    []     -> error "Empty list given to unsafeUnsnoc."
    (x:xs) -> unsnoc x xs
      where
        unsnoc :: a -> [a] -> (a, [a])
        unsnoc y = \case
            []     -> (y, [])
            (z:zs) -> (y :) <$> unsnoc z zs

{- | Same as 'parMap', but split the given list into chunks of size n,
   i.e. instead of spawning a thread for every element of the list we do so for
   chunks of n elements.  This prevents your computer from melting a hole
   through your desk when e.g. running the @parse@ command without any
   optimizations enabled.
-}
parMapN :: Int -> Strategy b -> (a -> b) -> [a] -> [b]
parMapN n strat f = (`using` parListChunk n strat) . map f

-- For the following, please note that streams are written in covariant notation
-- (as is common).

{- | Run the 'Identity' monad, i.e. just unwrap a by applying the obvious
   isomorphism Identity a ≅ a.  As the name implies, this will be used when
   evaluating pure streams.
-}
runStream :: Identity a -> a
runStream = runIdentity

-- | Stream from stdin to stdout, applying a transformation to the trees after
-- parsing them.  See Note [Memory]
streamInOut :: (PennTree -> PennTree) -> IO ()
streamInOut = streamToStdout SB.stdin

-- | Stream from a given file to stdout, applying a transformation to the trees
-- after parsing them.  See Note [Memory]
streamFileOut :: FilePath -> (PennTree -> PennTree) -> IO ()
streamFileOut file = runResourceT . streamToStdout (SB.readFile file)

-- | Stream to stdout, transforming the parsed trees in some way before pretty
-- printing them again.  See Note [Memory]
streamToStdout :: MonadIO m => ByteStream m r -> (PennTree -> PennTree) -> m ()
streamToStdout prep transTree =
    streamParse prep pTree $ S.map (transTree >>> prettyTree >>> (<> "\n"))
                         >>> SB.fromChunks
                         >>> SB.stdout

{- | Worker function for stream parsing and extracting the output.

   The 'parsed' function parses until it either reaches the end of the given
   input, or a parse failure occurs.  In case of a parse failure, we simply
   discard everything beyond that point and extract the input we have been able
   to parse correctly up until that point.
-}
streamParse
    :: Monad m => ByteStream m r -> Parser a -> (Stream (Of a) m () -> b) -> b
streamParse stream parser extract
    = stream
    & parsed parser
    & void           -- Discard unconsumed input.
    & extract

{- Note [Memory]
   ~~~~~~~~~~~~~~~~~~~~~~
   These are situations where we can guarantee streaming the entire way through.
   As we are only applying a pure transformation to the trees before writing
   them back to stdout or some file handle, there is no need to ever break
   streaming and work with the data in some other way (e.g. by creating a map
   with some additional information).  This means that these functions only need
   constant memory*.

   * We assume that the functions are used in good faith, i.e. all of this is
     modulo type-system subversion via 'unsafePerformIO' and similar
     shenanigans.
-}
