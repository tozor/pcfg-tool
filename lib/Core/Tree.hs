{-# LANGUAGE StrictData #-}

{- |
   Module      : Core.Tree
   Description : Tree type and utility functions pertaining to it.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Core.Tree
    ( -- * Types and patterns
      Tree(.., Leaf, Preterminal)  -- instances: Show, Eq
    , PennTree                     -- type alias: Tree ByteString
    , PennForest                   -- type alias: [PennTree]

      -- * Utility functions
    , prettyTree                   -- :: PennTree -> ByteString
    , leaves                       -- :: Tree a -> [a]
    , mapLeaves                    -- :: (a -> a) -> Tree a -> Tree a
    ) where

import qualified Data.ByteString as BS

{- | General, value strict, type for a tree.
   This type will be used all over the place, e.g. this is the type that a given
   S-expression will be parsed into.
-}
data Tree a = Node
    { root      :: a
    , subForest :: [Tree a]
    } deriving (Show, Eq)

-- | A single leaf of a tree.  Bidirectional pattern, same as @Node r []@.
pattern Leaf :: a -> Tree a
pattern Leaf r = Node r []

-- | A preterminal.  Bidirectional pattern, same as @Node r [Node r' []]@.
pattern Preterminal :: a -> a -> Tree a
pattern Preterminal r r' = Node r [Node r' []]

{- | Convenient type aliases for the kind of trees we will mainly be working
   with.

   The corpus we are using consists solely of English sentences, the Penn
   Treebank is ASCII based, and we are streaming the input, hence we choose
   'ByteString' over 'Text' here.
-}
type PennTree   = Tree ByteString
type PennForest = [PennTree]

{- | Given a 'PennTree', produce a 'ByteString' that pretty prints the given
   tree as an S-expression.

   This is a right inverse of 'Grammar.Parser.pTree'.  If one restricts the
   S-expressions to a stricter form regarding whitespace (i.e. there is only a
   single space separating words from opening parenthesis (on the right) or
   other words), it becomes a left inverse as well.
-}
prettyTree :: PennTree -> ByteString
prettyTree = \case
    Leaf r -> "(" <> r <> ")"
    t      -> BS.drop 1 $ go t
  where
    go :: PennTree -> ByteString
    go = \case
        Leaf r    -> " " <> r
        Node r sf ->
            mconcat [" (", r, foldl' (\r' sf' -> r' <> go sf') "" sf, ")"]

-- | Flatten all the leaves from a tree into a list.
-- Inspired by 'Data.Tree.flatten', adjusted for a value strict tree type.
leaves :: Tree a -> [a]
leaves = go []
  where
    go :: [a] -> Tree a -> [a]
    go xs = \case
        Leaf r    -> r : xs
        Node _ sf -> foldl' go xs sf

-- | Apply a function to all leaves of a tree.
mapLeaves :: forall a. (a -> a) -> Tree a -> Tree a
mapLeaves f = go
  where
    go :: Tree a -> Tree a
    go = \case
        Leaf r    -> Leaf (f r)
        Node r sf -> Node r (map go sf)
