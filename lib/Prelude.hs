{-# OPTIONS_GHC -Wno-missing-import-lists #-}

{- |
   Module      : Prelude
   Description : Replace the build-in prelude with relude.
   Copyright   : (c) Tony Zorman, 2020
   License     : AGPL
   Maintainer  : tony.zorman@tu-dresden.de
   Stability   : experimental
   Portability : non-portable
-}
module Prelude
    ( module Relude
    , module Relude.Extra.Newtype
    , dup                          -- :: a -> (a, a)
    , toList                       -- :: IsList a => a -> [GHC.Exts.Item a]
    ) where

{- Relude is a much more convenient alternative to the standard prelude.  It
   attempts to fix some of the more dire issues in prelude (e.g. partial
   functions, the 'String' problem) and automatically exports most important
   modules from base (thus cleaning up import lists), with some idiomatic
   combinators thrown in.  It also has a small dependency footprint (indeed, we
   depend on every single one of its dependencies already, thus adding it does
   not pull in any unnecessary packages).  In our opinion, these conveniences
   outweigh the cost of using an alternative prelude (at least for this
   application).
-}
import Relude hiding (toList)
import Relude.Extra.Newtype
import Relude.Extra.Tuple (dup)

{- Relude uses 'GHC.Exts.fromList', but 'Data.Foldable.toList' by default.  We
   find this very unintuitive and so we instead export 'GHC.Exts.toList' as the
   default 'toList' function.
-}
import GHC.Exts (toList)
